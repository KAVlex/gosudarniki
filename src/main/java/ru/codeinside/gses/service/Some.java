package ru.codeinside.gses.service;

public class Some<T> {

	final boolean present;
	final T value;

	private Some(boolean present, T value) {
		this.present = present;
		this.value = value;
	}

	public static <T> Some<T> empty() {
		return new Some<T>(false, null);
	}

	public static <T> Some<T> of(T value) {
		return new Some<T>(true, value);
	}

	public boolean isPresent() {
		return present;
	}

	public T get() {
		if (!present) {
			throw new IllegalStateException();
		}
		return value;
	}
}
