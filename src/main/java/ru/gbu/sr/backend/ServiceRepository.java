package ru.gbu.sr.backend;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ru.gbu.sr.backend.data.entity.Service;

public interface ServiceRepository extends JpaRepository<Service, Long>, JpaSpecificationExecutor<Service>  {

}
