package ru.gbu.sr.backend;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ru.gbu.sr.backend.data.entity.ProjectEstimate;

public interface ProjectEstimateRepository extends JpaRepository<ProjectEstimate, Long>, JpaSpecificationExecutor<ProjectEstimate>  {

}
