package ru.gbu.sr.backend;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.backend.data.entity.Organization;

public interface EmployeeRepository extends JpaRepository<Employee, String>, JpaSpecificationExecutor<Employee> {

	public Employee findByLogin(String login);

	public Employee findBySnils(String snils);

	public List<Employee> findByOrganization(Organization organization);
	
	public List<Employee> findByLockedFalse();

}
