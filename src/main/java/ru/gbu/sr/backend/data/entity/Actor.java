package ru.gbu.sr.backend.data.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Actor implements Serializable {

  private static final long serialVersionUID = 1L;

  private String name;
  private String ip;
  private String os;
  private String browser;


  protected Actor() {
  }

  public Actor(final String name, final String ip, final String os, final String browser) {
    this.name = name;
    this.ip = ip;
    this.os = os;
    this.browser = browser;
  }

  public String getName() {
    return name;
  }

  public String getIp() {
    return ip;
  }

  public String getOs() {
    return os;
  }


  public String getBrowser() {
    return browser;
  }

  @Override
  public String toString() {
    return name == null ? ip : name;
  }
}
