package ru.gbu.sr.backend.data;

import java.time.LocalDateTime;
import java.util.Set;

public class EmployeeFind {

    String organization;
    String lastName;
    String snils;
    LocalDateTime dateStart;
    LocalDateTime dateEnd;
    Boolean active = true;
    Set<Role> roles;

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public LocalDateTime getDateStart() {
        return dateStart;
    }

    public void setDateStart(LocalDateTime dateStart) {
        this.dateStart = dateStart;
    }

    public LocalDateTime getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(LocalDateTime dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    
    public Set<Role> getRoles() {
        return roles;
    }
 
    public void setRoles(Set<Role> userRoles) {
        this.roles = userRoles;
    }
}
