package ru.gbu.sr.backend.data.entity;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public boolean isNew() {
		return getId() == null;
	}

	abstract Long getId();

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}

		return 31 + getId().hashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (getId() == null) {
			// New entities are only equal if the instance if the same
			return super.equals(other);
		}

		if (this == other) {
			return true;
		}
		if (!(other instanceof AbstractEntity)) {
			return false;
		}
		return getId().equals(((AbstractEntity) other).getId());
	}
	

}
