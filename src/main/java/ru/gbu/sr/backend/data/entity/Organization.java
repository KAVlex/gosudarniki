package ru.gbu.sr.backend.data.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import ru.gbu.sr.backend.service.Logger;


@Entity
@EntityListeners(Logger.class)
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "parent_id", "name" }))
@NamedQueries({ @NamedQuery(name = "findAllOrganizations", query = "SELECT o FROM Organization o ORDER BY o.name"),
		@NamedQuery(name = "findAllOrganizationIds", query = "SELECT o.id FROM Organization o ORDER BY o.id") })
@SequenceGenerator(name = "organization_seq", sequenceName = "organization_seq")
public class Organization extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "organization_seq")
	private Long id;

	@Column(nullable = false)
	private String tel;

	@Column(nullable = false)
	private String email;
	
	@Column
	private String kbk;

	@Column
	private String short_name;

	@Column(nullable = false)
	private String name;

	@Column
	private String kpp;

	@Column
	private String urn;

	@Column(nullable = false)
	private String ogrn;

	@Column
	private String rs;

	@Column
	private String ls;

	@Column
	private String ks;

	@Column
	private String bank_name;

	@Column
	private String bik;

	@Column
	private String jaddr;

	@Column
	private String paddr;
	
	@Column(nullable = false)
	private String oktmo;
	
	@Column(nullable = false)
	private String inn;

	@ManyToOne(fetch = FetchType.LAZY)
	private Organization parent;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date = new Date();

	@ManyToOne(fetch = FetchType.LAZY)
	private Employee creator;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
	private Set<Organization> organizations;

	@OneToMany(mappedBy = "organization", fetch = FetchType.LAZY)
	private Set<Employee> employees;

	@Column(nullable = false)
	private boolean locked;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Employee updater;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateUpdated = new Date();
	
	@Column(nullable = true)
	private String registryNumber;
	
	public String getKbk() {
		return kbk;
	}

	public void setKbk(String kbk) {
		this.kbk = kbk;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getShortName() {
		return short_name;
	}

	public void setShortName(String shortName) {
		this.short_name = shortName;
	}

	public String getKpp() {
		return kpp;
	}

	public void setKpp(String kpp) {
		this.kpp = kpp;
	}

	public String getUrn() {
		return urn;
	}

	public void setUrn(String urn) {
		this.urn = urn;
	}

	public String getOgrn() {
		return ogrn;
	}

	public void setOgrn(String ogrn) {
		this.ogrn = ogrn;
	}

	public String getRs() {
		return rs;
	}

	public void setRs(String rs) {
		this.rs = rs;
	}

	public String getLs() {
		return ls;
	}

	public void setLs(String ls) {
		this.ls = ls;
	}

	public String getKs() {
		return ks;
	}

	public void setKs(String ks) {
		this.ks = ks;
	}

	public String getBankName() {
		return bank_name;
	}

	public void setBankName(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getBik() {
		return bik;
	}

	public void setBik(String bik) {
		this.bik = bik;
	}

	public String getJaddr() {
		return jaddr;
	}

	public void setJaddr(String jaddr) {
		this.jaddr = jaddr;
	}

	public String getPaddr() {
		return paddr;
	}

	public void setPaddr(String paddr) {
		this.paddr = paddr;
	}

	public String getOktmo() {
		return oktmo;
	}

	public void setOktmo(String oktmo) {
		this.oktmo = oktmo;
	}

	public String getInn() {
		return inn;
	}

	public void setInn(String inn) {
		this.inn = inn;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	public Employee getCreator() {
		return creator;
	}

	public void setCreator(Employee creator) {
		this.creator = creator;
	}

	public Set<Employee> getEmployees() {
		if (employees == null) {
			employees = new HashSet<Employee>();
		}
		return employees;
	}

	public Organization getParent() {
		return parent;
	}

	public void setParent(Organization parent) {
		this.parent = parent;
	}

	public Set<Organization> getOrganizations() {
		if (organizations == null) {
			organizations = new HashSet<Organization>();
		}
		return organizations;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public String getRegistryNumber() {
		return registryNumber;
	}

	public void setRegistryNumber(String registryNumber) {
		this.registryNumber = registryNumber;
	}

	public Employee getUpdater() {
		return updater;
	}

	public void setUpdater(Employee updater) {
		this.updater = updater;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

}
