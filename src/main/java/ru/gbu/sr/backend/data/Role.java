package ru.gbu.sr.backend.data;

public enum Role {

	Administrator("Администратор"),

	Manager("Менеджер"),

	Executor("Исполнитель"),

	Supervisor("Контролер"),

	SuperSupervisor("Общий контролер"),

	Declarant("Заявитель"),

	Operator("Оператор"),
	
	Programmer("Программист");

	final public String description;

	Role(String description) {
		this.description = description;
	}

	public static String[] getAllRoles() {
		return new String[] { Administrator.name(), Manager.name(), Executor.name(), Supervisor.name(),
				SuperSupervisor.name(), Declarant.name(), Operator.name() };
	}
	
	public String getDescription() {
		return description;
	}
}
