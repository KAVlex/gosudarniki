package ru.gbu.sr.backend.data.entity;

public enum DefinitionStatus {
	
	Work("Обсуджение"), 
	Expert("Экспертиза"), 
	Debugging("Голосование"), 
	Reject("Отклонен"), 
	Accept("Одобрен"),
	Relized("Реализован"),
	Archive("Архив");
	
	private String name;

	DefinitionStatus(String name) {
		this.name = name;
	}

	public String getLabelName() {
		return name;
	}
	
	public String getName() {
		return name;
	}

	public static DefinitionStatus getStatusByLabelName(String labelName) {
		for (DefinitionStatus s : DefinitionStatus.values()) {
			if (s.getLabelName().equals(labelName)) {
				return s;
			}
		}
		return DefinitionStatus.Expert;
	}

	
}