package ru.gbu.sr.backend.data.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import ru.gbu.sr.backend.service.Logger;


@Entity
@EntityListeners(Logger.class)
@NamedQuery(name = "findAllServices", query = "SELECT s FROM Service s")
@SequenceGenerator(name = "service_seq", sequenceName = "service_seq")
public class Service extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 222L;

	@Id
	@GeneratedValue(generator = "service_seq")
	private Long id;

	@Column(length = 1500)
	private String name;
	
	@Column(length = 3000)
	private String descripiton;
	
	@Column
	private String type;
	
	@Column
	private String address;
	
	@Column
	private String url_conf;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMeeting;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreated = new Date();

	@ManyToOne(fetch = FetchType.LAZY)
	private Employee creator;

	@ElementCollection(targetClass = String.class)
	@CollectionTable(name = "service_declarant_type")
	@Column(name = "declarant_type")
	private List<String> declarantTypes;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Employee updater;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateUpdated = new Date();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setCreator(Employee creator) {
		this.creator = creator;
	}

	@Override
	public String toString() {
		return getId() != null ? getId().toString() : "";
	}


	public Date getDateMeeting() {
		return dateMeeting;
	}

	public void setDateMeeting(Date dateMeeting) {
		this.dateMeeting = dateMeeting;
	}

	public Employee getUpdater() {
		return updater;
	}

	public void setUpdater(Employee updater) {
		this.updater = updater;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
}