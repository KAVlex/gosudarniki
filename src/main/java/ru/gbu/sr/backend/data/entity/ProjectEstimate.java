package ru.gbu.sr.backend.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@SequenceGenerator(name = "project_estimate_seq", sequenceName = "project_estimate_seq")
@JsonIgnoreProperties("project")
public class ProjectEstimate extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 222L;

	@Id
	
	@GeneratedValue(generator = "project_estimate_seq")
	private Long id;

	@Column
	private String name;
	
	@Column
	private Long count;
	
	@Column
	private Long price;
	
	@JoinColumn(nullable = false)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Project project;
	
	@Override
	Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}
	
	public Long getCost() {
		return this.price * this.count;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
