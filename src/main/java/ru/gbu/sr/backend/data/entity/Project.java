package ru.gbu.sr.backend.data.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import ru.gbu.sr.backend.service.Logger;


@Entity
@EntityListeners(Logger.class)
@SequenceGenerator(name = "project_seq", sequenceName = "project_seq")
public class Project extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 222L;

	@Id
	@GeneratedValue(generator = "project_seq")
	private Long id;

	@Column
	private String name;
	
	@Column
	private String description;
	
	@Column
	private String target;
	
	@Column
	private String task;
	
	@Column
	private String sphere;
	
	@Column
	private String address;
	
	@Column
	private String status;
	
	@Column
	@Enumerated(EnumType.STRING)
	private DefinitionStatus defstatus;
	
	@Column
	private String munitipality_level;
	
	@Column
	private String initiator;
	
	@Column
	private String expected_results;
	
	@OneToMany(mappedBy = "project", fetch = FetchType.LAZY, cascade={CascadeType.ALL})
	private Set<ProjectEstimate> estimates;
	
	public String getExpected_results() {
		return expected_results;
	}

	public void setExpected_results(String expected_results) {
		this.expected_results = expected_results;
	}

	@Column
	private Long cost;
	
	@Column
	private Long rating;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreated = new Date();

	@ManyToOne(fetch = FetchType.LAZY)
	private Employee creator;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Employee updater;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateUpdated = new Date();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setCreator(Employee creator) {
		this.creator = creator;
	}

	@Override
	public String toString() {
		return getId() != null ? getId().toString() : "";
	}

	public Long getRating() {
		return rating;
	}

	public void setRating(Long rating) {
		this.rating = rating;
	}

	public Employee getUpdater() {
		return updater;
	}

	public void setUpdater(Employee updater) {
		this.updater = updater;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getSphere() {
		return sphere;
	}

	public void setSphere(String sphere) {
		this.sphere = sphere;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMunitipality_level() {
		return munitipality_level;
	}

	public void setMunitipality_level(String munitipality_level) {
		this.munitipality_level = munitipality_level;
	}

	public String getInitiator() {
		return initiator;
	}

	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}

	public Long getCost() {
		return cost;
	}

	public void setCost(Long cost) {
		this.cost = cost;
	}

	public Employee getCreator() {
		return creator;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<ProjectEstimate> getEstimates() {
		return estimates;
	}

	public void setEstimates(Set<ProjectEstimate> estimates) {
		this.estimates = estimates;
	}

	public DefinitionStatus getDefstatus() {
		return defstatus;
	}

	public void setDefstatus(DefinitionStatus defstatus) {
		this.defstatus = defstatus;
	}
	
	public String getDefstatusName() {
		return defstatus.getLabelName();
	}
	
}