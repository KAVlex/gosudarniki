package ru.gbu.sr.backend.data;

public enum SigPluginType {
	
	JCP(0, "КриптоПро JCP"),
	CProBrowserPlugin(1, "КриптоПро Browser Plug-In"),
	NONE(100, "Не выбран");
	
	private final Integer id;
	private final String caption;
	
	SigPluginType(Integer id, String caption) {
		this.id = id;
		this.caption = caption;
	}

	public Integer getId() {
		return id;
	}

	public String getCaption() {
		return caption;
	}
	
	public static SigPluginType fromId(Integer id) {
		if (id == null)
			return NONE;

		for (SigPluginType sigPlugin : SigPluginType.values()) {
			if (sigPlugin.id.equals(id)) {
				return sigPlugin;
			}
		}
		
		return NONE;
	}
		
}
