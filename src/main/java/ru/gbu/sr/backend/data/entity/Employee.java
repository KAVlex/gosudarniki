package ru.gbu.sr.backend.data.entity;

import org.apache.commons.codec.digest.DigestUtils;

import ru.gbu.sr.backend.data.Role;
import ru.gbu.sr.backend.service.Logger;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.EnumSet;
import java.util.Set;
import java.util.TreeSet;

@Entity
@EntityListeners(Logger.class)
@NamedQueries({ @NamedQuery(name = "findAllEmployees", query = "SELECT u FROM Employee u"),
		@NamedQuery(name = "findAllEmployeeLogins", query = "SELECT u.login FROM Employee u") })
public class Employee implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 64)
	private String login;

	@Column(nullable = false)
	private String passwordHash;

	@Column(length = 11)
	private String snils;

	private String status;

	@Column(name = "last_name", length = 32)
	private String lastName;

	@Column(name = "first_name", length = 32)
	private String firstName;

	@Column(name = "patronymic", length = 32)
	private String patronymic;

	@Column(name = "birth_date")
	@Temporal(TemporalType.DATE)
	private Date birthDate;

	@Column(name = "position", length = 64)
	private String position;

	@Column(name = "contact_phone", length = 16)
	private String contactPhone;

	@Column(name = "inn", length = 12)
	private String inn;

	@Column(name = "email", length = 64)
	private String email;

	@Column(name = "notifications", nullable = true)
	private Boolean notifications;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date = new Date();

	private String creator;

	@Column(name = "sig_plugin")
	private Integer sigPlugin;

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "roles", joinColumns = @JoinColumn(name = "uid"), uniqueConstraints = @UniqueConstraint(columnNames = {
			"uid", "gid" }))
	@Enumerated(EnumType.STRING)
	@Column(name = "gid", length = 32)
	private Set<Role> roles;

	@JoinColumn(nullable = false)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Organization organization;

	@Column(nullable = false)
	private boolean locked;

	@OneToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	CertificateOfEmployee certificate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date unlockTime;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Employee updater;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateUpdated = new Date();

	public boolean isLocked() {
		return locked;
	}

	public Employee getUpdater() {
		return updater;
	}

	public void setUpdater(Employee updater) {
		this.updater = updater;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public Set<Role> getRoles() {
		if (roles == null) {
			roles = EnumSet.noneOf(Role.class);
		}
		return roles;
	}

	public Employee() {
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public String getMaskedContactPhone() {
		if (this.contactPhone != null && !this.contactPhone.isEmpty())
			return "+7(".concat(this.contactPhone.substring(0, 3)).concat(")")
					.concat(this.contactPhone.substring(3, 6).concat("-")).concat(this.contactPhone.substring(6));
		return this.contactPhone;

	}

	public void setContactPhone(String contactPhone) {
		if (contactPhone != null && contactPhone.startsWith("+7("))
			contactPhone = contactPhone.replace("+7(", "").replace(")", "").replace("-", "");

		this.contactPhone = contactPhone;
	}

	public String getInn() {
		return inn;
	}

	public void setInn(String inn) {
		this.inn = inn;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getNotifications() {
		return this.notifications;
	}

	public void setNotifications(Boolean notifications) {
		this.notifications = notifications;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private static final String DELIM = " ";

	public String getFio() {
		String fio = this.lastName.concat(DELIM).concat(this.firstName);
		if (this.patronymic != null && !this.patronymic.isEmpty()) {
			fio = fio.concat(DELIM).concat(this.patronymic);
		}
		return fio;
	}

	public void setFio(String fio) {

		String[] split = fio.split(DELIM);
		if (split.length > 0)
			this.lastName = split[0];
		else {
			this.lastName = "";
			this.firstName = "";
			this.patronymic = "";
		}

		if (split.length > 1)
			this.firstName = split[1];
		else {
			this.firstName = "";
			this.patronymic = "";
		}

		if (split.length > 2) {
			this.patronymic = split[2];
			for (int i = 3; i < split.length; i++) {
				this.patronymic += DELIM.concat(split[i]);
			}
		} else
			this.patronymic = "";
	}

	public Date getDate() {
		return date;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Set<String> getRoleNames() {
		final TreeSet<String> result = new TreeSet<String>();
		for (Role role : getRoles()) {
			result.add(role.description);
		}
		return result;
	}

	public CertificateOfEmployee getCertificate() {
		return certificate;
	}

	public void setCertificate(CertificateOfEmployee certificate) {
		this.certificate = certificate;
	}

	public String getSnils() {
		return snils;
	}

	public String getMaskedSnils() {
		if (this.snils != null && !this.snils.isEmpty())
			return this.snils.substring(0, 3).concat("-").concat(this.snils.substring(3, 6).concat("-"))
					.concat(this.snils.substring(6, 9).concat("-")).concat(this.snils.substring(9));
		return this.snils;
	}

	public void setSnils(String snils) {
		snils = snils.replaceAll("-", "");
		this.snils = snils;
	}

	public boolean checkPassword(String password) {
		String hex = DigestUtils.sha256Hex(password);
		return passwordHash.equals(hex);
	}

	
	public boolean isNew() {
		return login == null;
	};

	@Embeddable
	public class PersonInfo {
		@Column(name = "first_name", nullable = false)
		private String firstName;

		@Column(name = "last_name", nullable = false)
		private String lastName;

		@Column(name = "patronymic")
		private String patronymic = "";

		public PersonInfo(String firstName, String lastName) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		public PersonInfo(String firstName, String lastName, String patronymic) {
			this(firstName, lastName);
			if (patronymic != null)
				this.patronymic = patronymic;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getPatronymic() {
			return patronymic;
		}

		public void setPatronymic(String patronymic) {
			if (patronymic == null)
				this.patronymic = "";
			else
				this.patronymic = patronymic;
		}
	}

}
