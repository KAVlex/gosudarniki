package ru.gbu.sr.backend.service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.blueconic.browscap.Capabilities;
import com.blueconic.browscap.UserAgentParser;
import com.blueconic.browscap.UserAgentService;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.spring.annotation.SpringComponent;

import ru.gbu.sr.app.StaticContextAccessor;
import ru.gbu.sr.app.security.SecurityUtils;
import ru.gbu.sr.backend.data.entity.Actor;
import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.backend.data.entity.Log;
import ru.gbu.sr.backend.data.entity.Organization;
import ru.gbu.sr.backend.data.entity.Project;

@SpringComponent
public class Logger {

	private static UserAgentParser parser;

	static {
		try {
			Logger.parser = new UserAgentService().loadParser();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@PostConstruct
	public void init() {
		System.out.println("Initializing Logger");
	}

	private static final String PERSIST = "Persist";
	private static final String UPDATE = "Update";
	private static final String REMOVE = "Remove";

	@PostPersist
	public void persist(Object obj) {
		createLog(obj, PERSIST);
	}

	@PostUpdate
	public void update(Object obj) {
		createLog(obj, UPDATE);
	}

	@PostRemove
	public void remove(Object obj) {
		createLog(obj, REMOVE);
	}

	public static void createLog(Object obj, String action) {

		if (VaadinService.getCurrentRequest() != null && parser != null) {
			Actor actor = createActor(VaadinService.getCurrentRequest());
			String entityName = obj.getClass().getSimpleName();
			String entityId = getEntityId(obj);
			String info = null;

			createLog(actor, entityName, entityId, action, info, true);
		}
	}

	public static Actor createActor(VaadinRequest request) {
		final String userAgent = request.getHeader("user-agent");
		final Capabilities capabilities = parser.parse(userAgent);
		final String platform = capabilities.getDeviceType() + " " + capabilities.getPlatform() + ", "
				+ capabilities.getPlatformVersion();
		final String browser = capabilities.getBrowser() + " " + capabilities.getBrowserMajorVersion();
		Actor actor = new Actor(SecurityUtils.getUsername(), request.getRemoteAddr(), platform, browser);
		return actor;
	}

	public static void createLog(Actor actor, String entityName, String entityId, String action, String info,
			Boolean actionResult) {
		Log log = new Log(actor, entityName, entityId, action, info, actionResult);
		EntityManager entityManager = StaticContextAccessor.getBean(EntityManager.class);
		PlatformTransactionManager transactionManager = StaticContextAccessor.getBean(PlatformTransactionManager.class);
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				entityManager.persist(log);
			}
		});
	}

	private static String getEntityId(Object obj) {
		String entityId = "unknow";

		// TODO: использовать метаданные!
		if (obj instanceof Employee) {
			Employee employee = (Employee) obj;
			entityId = employee.getLogin();

		} else if (obj instanceof Organization) {
			Organization organization = (Organization) obj;
			entityId = organization.getId().toString();
		} else if (obj instanceof Project) {
			Project service = (Project) obj;
			entityId = service.getId().toString();
		}
		return entityId;
	}
}