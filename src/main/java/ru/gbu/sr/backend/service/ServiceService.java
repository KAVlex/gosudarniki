package ru.gbu.sr.backend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import ru.gbu.sr.app.security.SecurityUtils;
import ru.gbu.sr.backend.ServiceRepository;
import ru.gbu.sr.backend.data.ServiceFind;
import ru.gbu.sr.backend.data.entity.Service;

@org.springframework.stereotype.Service
public class ServiceService extends CrudService<Service> {

	private final ServiceRepository serviceRepository;

	@Autowired
	public ServiceService(ServiceRepository serviceRepository) {
		this.serviceRepository = serviceRepository;
	}

	public ServiceRepository getRepository() {
		return serviceRepository;
	}

	public Service findById(Long id) {
		return getRepository().findById(id).orElse(null);
	}

	public Service save(Service service) {
		if (service.getId() == null) {
			service.setCreator(SecurityUtils.getCurrentUser());
		}
		service.setUpdater(SecurityUtils.getCurrentUser());
		service.setDateUpdated(new Date());
		return getRepository().save(service);
	}

	public List<Service> findActive() {
		return getRepository().findAll();
	}

	@Override
	public long countAnyMatching(Object filter) {
		return getRepository().count(isFilterService((ServiceFind) filter));
	}

	@Override
	public Page<Service> findAnyMatching(Object filter, Pageable pageable) {
		return getRepository().findAll(isFilterService((ServiceFind) filter), pageable);
	}
	
	public static Specification<Service> isFilterService(ServiceFind serviceFind) {
		return new Specification<Service>() {
			private static final long serialVersionUID = 1L;

			public Predicate toPredicate(Root<Service> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				List<Predicate> predicatas = new ArrayList<>();
				if (serviceFind != null) {
					predicatas.add(builder.like(builder.lower(root.get("name")), wrap(serviceFind.getName())));

				}
				return builder.and(predicatas.toArray(new Predicate[predicatas.size()]));
			}

			private String wrap(String val) {
				return (val != null ? "%" + val.toLowerCase() + "%" : "%%");
			};
		};
	}
}
