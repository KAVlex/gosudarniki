package ru.gbu.sr.backend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import ru.gbu.sr.app.security.SecurityUtils;
import ru.gbu.sr.backend.OrganizationRepository;
import ru.gbu.sr.backend.data.OrganizationFind;
import ru.gbu.sr.backend.data.entity.Organization;
import ru.gbu.sr.backend.util.DateUtils;

@Service
public class OrganizationService extends CrudService<Organization> {

	private final OrganizationRepository organizationRepository;
	private final EmployeeService employeeService;
	
	public OrganizationService(OrganizationRepository organizationRepository, EmployeeService employeeService) {
		this.organizationRepository = organizationRepository;
		this.employeeService = employeeService;
	}
	
	@Override
	protected CrudRepository<Organization, Long> getRepository() {
		return organizationRepository;
	}

	@Override
	public long countAnyMatching(Object filter) {
		return organizationRepository.count(isFilterOrg((OrganizationFind)filter));
	}

	@Override
	public Page<Organization> findAnyMatching(Object filter, Pageable pageable) {
		return organizationRepository.findAll(isFilterOrg((OrganizationFind)filter), pageable);
	}
	
	public List<Organization> findAll(){
		return organizationRepository.findAll();
	}
	
	public static Specification<Organization> isFilterOrg(OrganizationFind organization) {
		return new Specification<Organization>() {
			private static final long serialVersionUID = 1L;

			public Predicate toPredicate(Root<Organization> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				List<Predicate> predicatas = new ArrayList<>();
				if (organization != null) {
					predicatas.add(builder.like(builder.lower(root.get("name")), wrap(organization.getName())));
					predicatas.add(builder.like(root.get("id").as(String.class), wrap(organization.getId())));
					if (organization.getRegistryNumber() != null && !organization.getRegistryNumber().isEmpty()) {
						predicatas.add(builder.like(root.get("registryNumber"), wrap(organization.getRegistryNumber())));	
					}
					if (organization.getOktmo() != null && !organization.getOktmo().isEmpty()) {
						predicatas.add(builder.like(root.get("oktmo"), wrap(organization.getOktmo())));	
					}
					if (organization.getInn() != null && !organization.getInn().isEmpty()) {
						predicatas.add(builder.like(root.get("inn"), wrap(organization.getInn())));	
					}
					if (organization.getStartDate() != null) {
						predicatas.add(builder.greaterThanOrEqualTo(root.get("date"), DateUtils.asDate(organization.getStartDate())));
					}
					if (organization.getEndDate() != null) {
						predicatas.add(builder.lessThanOrEqualTo(root.get("date"), DateUtils.asDate(organization.getEndDate())));
					}
					if (organization.getActive()) {
						predicatas.add(builder.equal(root.get("locked"), false));
					}					
				}
				return builder.and(predicatas.toArray(new Predicate[predicatas.size()]));
			}
			
			private String wrap(String val) {
				return (val != null ? "%" + val.toLowerCase() + "%" : "%%");
			};
		};
	}
	
	public Organization save(Organization organization) {
		if (organization.getId() == null) {
			organization.setCreator(SecurityUtils.getCurrentUser());
		}
		organization.setUpdater(SecurityUtils.getCurrentUser());
		organization.setDateUpdated(new Date());
		return organizationRepository.save(organization);
	}
	
	private boolean saveOrg(Organization organization) {
		try {
			save(organization);	
		} catch (Exception e2) {
			e2.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean lock(Organization organization) {
		organization.setLocked(true);
		if (organization.getEmployees() != null) {
			organization.getEmployees().forEach(e -> {
				e.setLocked(true);
				employeeService.save(e);
			});			
		}
		return saveOrg(organization);
	}
	
	public boolean unlock(Organization organization) {
		organization.setLocked(false);
		return saveOrg(organization);
	}
	
	public boolean lock(Long id) {
		Optional<Organization> org = organizationRepository.findById(id);
		if (org.isPresent()) {
			return lock(org.get());
		}
		return false;
	}
	
	public boolean unlock(Long id) {
		Optional<Organization> org = organizationRepository.findById(id);
		if (org.isPresent()) {
			return unlock(org.get());
		}
		return false;
	}
	
	public Organization findById(Long id) {
		return organizationRepository.findById(id).orElse(null);
	}

	public List<Organization> findAllActiveExcludeId(Long id){
		return organizationRepository.findByIdNotAndLockedFalse(id);
	}
	
	public List<Organization> findAllActive(){
		return organizationRepository.findByLockedFalse();
	}
}
