package ru.gbu.sr.backend.service;

import org.springframework.stereotype.Service;

import ru.gbu.sr.backend.LogRepository;
import ru.gbu.sr.backend.data.entity.Log;

@Service(value = "logService")
public class LogService {

	private final LogRepository logRepository;
	
	public LogService(LogRepository logRepository) {
		this.logRepository = logRepository;
	}
	
	public Log save(Log log) {
		return logRepository.save(log);
	}
}
