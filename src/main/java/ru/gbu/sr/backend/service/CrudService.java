package ru.gbu.sr.backend.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import ru.gbu.sr.backend.data.entity.AbstractEntity;

public abstract class CrudService<T extends AbstractEntity> {

	protected abstract CrudRepository<T, Long> getRepository();

	public T save(T entity) {
		return getRepository().save(entity);
	}

	public void delete(long id) {
		getRepository().deleteById(id);
	}

	public T load(long id) {
		return getRepository().findById(id).orElse(null);
	}

	public abstract long countAnyMatching(Object filter);

	public abstract Page<T> findAnyMatching(Object filter, Pageable pageable);

}
