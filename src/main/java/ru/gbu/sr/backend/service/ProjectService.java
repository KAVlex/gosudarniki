package ru.gbu.sr.backend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import ru.gbu.sr.app.security.SecurityUtils;
import ru.gbu.sr.backend.ProjectRepository;
import ru.gbu.sr.backend.data.ProjectFind;
import ru.gbu.sr.backend.data.entity.Project;

@org.springframework.stereotype.Service
public class ProjectService extends CrudService<Project> {

	private final ProjectRepository serviceRepository;

	@Autowired
	public ProjectService(ProjectRepository serviceRepository) {
		this.serviceRepository = serviceRepository;
	}

	public ProjectRepository getRepository() {
		return serviceRepository;
	}

	public Project findById(Long id) {
		return getRepository().findById(id).orElse(null);
	}

	public Project save(Project service) {
		if (service.getId() == null) {
			service.setCreator(SecurityUtils.getCurrentUser());
		}
		service.setUpdater(SecurityUtils.getCurrentUser());
		service.setDateUpdated(new Date());
		return getRepository().save(service);
	}

	public List<Project> findActive() {
		return getRepository().findAll();
	}

	@Override
	public long countAnyMatching(Object filter) {
		return getRepository().count(isFilterService((ProjectFind) filter));
	}

	@Override
	public Page<Project> findAnyMatching(Object filter, Pageable pageable) {
		return getRepository().findAll(isFilterService((ProjectFind) filter), pageable);
	}


	public static Specification<Project> isFilterService(ProjectFind serviceFind) {
		return new Specification<Project>() {
			private static final long serialVersionUID = 1L;

			public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				List<Predicate> predicatas = new ArrayList<>();
				if (serviceFind != null) {
					predicatas.add(builder.like(builder.lower(root.get("name")), wrap(serviceFind.getName())));
					predicatas.add(builder.like(root.get("rating").as(String.class), wrap(serviceFind.getRating())));
					if (serviceFind.getCost() != null && !serviceFind.getCost().isEmpty()) {
						predicatas.add(builder.like(root.get("cost").as(String.class),
								wrap(serviceFind.getCost())));						
					}
				}
				return builder.and(predicatas.toArray(new Predicate[predicatas.size()]));
			}

			private String wrap(String val) {
				return (val != null ? "%" + val.toLowerCase() + "%" : "%%");
			};
		};
	}
}
