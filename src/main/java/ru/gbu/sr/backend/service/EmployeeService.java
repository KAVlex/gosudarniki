package ru.gbu.sr.backend.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import ru.gbu.sr.app.security.SecurityUtils;
import ru.gbu.sr.backend.EmployeeRepository;
import ru.gbu.sr.backend.data.EmployeeFind;
import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.backend.data.entity.Organization;
import ru.gbu.sr.backend.util.DateUtils;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    protected EmployeeRepository getRepository() {
        return employeeRepository;
    }

    public long countAnyMatching(Object filter) {
        return employeeRepository.count(isFilterUser((EmployeeFind)filter));
    }

    public Page<Employee> findAnyMatching(Object filter, Pageable pageable) {
        return employeeRepository.findAll(isFilterUser((EmployeeFind)filter), pageable);
    }

    public static Specification<Employee> isFilterUser(EmployeeFind employee) {
        return new Specification<Employee>() {
            private static final long serialVersionUID = 1L;

            public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> predicatas = new ArrayList<>();
                predicatas.add(builder.like(builder.lower(root.get("organization").get("name")), wrap(employee.getOrganization())));
                predicatas.add(builder.like(builder.lower(root.get("lastName")), wrap(employee.getLastName())));
                if (employee.getSnils() != null && !employee.getSnils().isEmpty()) {
                	predicatas.add(builder.like(root.get("snils"), wrap(employee.getSnils())));	
                }
				if (employee.getRoles() != null && employee.getRoles().size() > 0) {
					predicatas.add(builder.in(root.join("roles")).value(employee.getRoles()));
				}
                if (employee.getDateStart() != null) {
                    predicatas.add(builder.greaterThanOrEqualTo(root.get("birthDate"), DateUtils.asDate(employee.getDateStart())));
                }
                if (employee.getDateEnd() != null) {
                    predicatas.add(builder.lessThanOrEqualTo(root.get("birthDate"), DateUtils.asDate(employee.getDateEnd())));
                }
                if (employee.getActive()) {
                    predicatas.add(builder.equal(root.get("locked"), false));
                }
                return builder.and(predicatas.toArray(new Predicate[predicatas.size()]));
            }

            private String wrap(String val) {
                return (val != null ? "%" + val.toLowerCase() + "%" : "%%");
            };
        };
    }

    public Employee save(Employee employee) {
		if (employee.isNew()) {
			employee.setCreator(SecurityUtils.getUsername());
		}
		employee.setUpdater(SecurityUtils.getCurrentUser());
		employee.setDateUpdated(new Date());
        return employeeRepository.save(employee);
    }

    private boolean saveUsr(Employee employee) {
        try {
            save(employee);
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean lock(Employee employee) {
        employee.setLocked(true);
        return saveUsr(employee);
    }

    public boolean unlock(Employee employee) {
        employee.setLocked(false);
        return saveUsr(employee);
    }
    
    public boolean lock(String login) {
        Employee employee = employeeRepository.findByLogin(login);
        if(employee != null){
            return lock(employee);
        }
        return false;
    }
 
    public boolean unlock(String login) {
        Employee employee = employeeRepository.findByLogin(login);
        if(employee != null){
        	return unlock(employee);
        }
        return false;
    }    

    public Employee findByLogin(String login) {
        return getRepository().findByLogin(login);
    }

    public Employee findBySnils(String snils) {
        return getRepository().findBySnils(snils);
    }

    public List<Employee> findByOrg(Organization organization){
        return getRepository().findByOrganization(organization);
    }
    
    public List<Employee> findAllActive(){
    	return getRepository().findByLockedFalse();
    }
    
    public boolean existLogin(String login){
        return findByLogin(login) != null;
    }
 
    public boolean existSnils(String snils){
        return findBySnils(snils) != null;
    }
}
