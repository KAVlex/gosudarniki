package ru.gbu.sr.backend;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import ru.gbu.sr.backend.data.entity.Log;

@Transactional
public interface LogRepository extends JpaRepository<Log, Long> {

}
