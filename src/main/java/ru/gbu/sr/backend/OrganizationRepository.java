package ru.gbu.sr.backend;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ru.gbu.sr.backend.data.entity.Organization;

public interface OrganizationRepository extends JpaRepository<Organization, Long>, JpaSpecificationExecutor<Organization> {
	
	public List<Organization> findByIdNotAndLockedFalse(Long id);
	
	public List<Organization> findByLockedFalse();
	
}
