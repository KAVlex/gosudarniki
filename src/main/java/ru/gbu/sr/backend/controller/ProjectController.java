package ru.gbu.sr.backend.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ru.gbu.sr.backend.ProjectEstimateRepository;
import ru.gbu.sr.backend.ProjectRepository;
import ru.gbu.sr.backend.data.entity.DefinitionStatus;
import ru.gbu.sr.backend.data.entity.Project;
import ru.gbu.sr.backend.data.entity.ProjectEstimate;

@RestController
public class ProjectController {

	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private ProjectEstimateRepository projectEstimateRepository;
	
	@GetMapping("api/project")
    public Project project(@RequestParam(value="id") Long id) {
		return projectRepository.findById(id).get();
    }
	
	@PostMapping(value = "api/project", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public Project saveProject(@RequestBody Project body) {
		Project project = new Project();
		project.setName(body.getName());
		project.setCost(body.getCost());
		project.setDescription(body.getDescription());
		project.setAddress(body.getAddress());
		project.setExpected_results(body.getExpected_results());
		project.setTask(body.getTask());
		project.setTarget(body.getTarget());
		project.setMunitipality_level(body.getMunitipality_level());
		project.setRating(body.getRating());
		project.setSphere(body.getSphere());
		project.setStatus(body.getStatus());
		project.setDefstatus(DefinitionStatus.Expert);
		project = projectRepository.save(project);
		Set<ProjectEstimate> estimates = body.getEstimates();
		for (ProjectEstimate projectEstimate : estimates) {
			projectEstimate.setProject(project);
			projectEstimateRepository.save(projectEstimate);
		}
		return project;
    }
    
}
