package ru.gbu.sr.app;

import com.vaadin.spring.access.SecuredViewAccessControl;

import ru.gbu.sr.app.security.CustomPasswordEncoder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@ComponentScan({"ru.gbu.sr.backend.controller"})
public class ApplicationConfiguration {
	
	@Autowired
	ApplicationContext applicationContext;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new CustomPasswordEncoder();
	}

	@Bean
	SecuredViewAccessControl securedViewAccessControl() {
		return new SecuredViewAccessControl();
	}
	
}
