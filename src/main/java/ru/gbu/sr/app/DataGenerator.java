package ru.gbu.sr.app;

import java.util.Date;
import java.util.Random;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.vaadin.spring.annotation.SpringComponent;

import ru.gbu.sr.backend.EmployeeRepository;
import ru.gbu.sr.backend.OrganizationRepository;
import ru.gbu.sr.backend.ProjectRepository;
import ru.gbu.sr.backend.data.Role;
import ru.gbu.sr.backend.data.entity.DefinitionStatus;
import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.backend.data.entity.Organization;
import ru.gbu.sr.backend.data.entity.Project;

@SpringComponent
public class DataGenerator implements HasLogger {
	
	private static final String[] FIRST_NAME = new String[] { "Иван", "Артем", "Астемир", "Тимур", "Георгий", "Илья",
			"Ильзат", "Александр", "Федор", "Даниил", "Максим", "Хетаг", "Виктор", "Павел", "Виталий", "Никита",
			"Константин", "Кирилл", "Дмитрий", "Ника", "Наир", "Сергей" };
	private static final String[] LAST_NAME = new String[] { "Иванов", "Петров", "Сидоров", "Скворцов", "Смирнов", "Дзюба",
			"Гордюшенко", "Жамелетдинов", "Игнатович", "Макаров", "Чернов", "Маклаков", "Кучаев", "Белов", "Янов", "Тикнизян",
			"Жиронкин", "Пилиев", "Ефремов", "Головин", "Обляков", "Чалов", "Набабкин", "Щенников", "Помазун", "Ахметов", "Хосонов",
			"Елеев", "Пухов", "Овчинноков", "Онопко", "Гончаренко", "Гурин", "Головлев" };
	
	private final Random random = new Random(1L);
	
	private <T> T getRandom(T[] array) {
		return array[random.nextInt(array.length)];
	}

	@Bean
	public CommandLineRunner loadData(PasswordEncoder passwordEncoder, OrganizationRepository organizationRepository, 
			EmployeeRepository employeeRepository, ProjectRepository serviceRepository) {
		return args -> {
			if (hasData(employeeRepository)) {
				getLogger().info("Using existing database");
				createOperator(organizationRepository, employeeRepository, passwordEncoder);
				return;
			}

			getLogger().info("Generating demo data");
			createProjects(serviceRepository);
			createOperator(organizationRepository, employeeRepository, passwordEncoder);
			createOrganizations(organizationRepository, employeeRepository, passwordEncoder);
			createEmployees(employeeRepository, passwordEncoder);

			getLogger().info("Generated demo data");
		};
	}

	private boolean hasData(EmployeeRepository userRepository) {
		return userRepository.count() != 0L;
	}
	
	private void createOrganizations(OrganizationRepository organizationRepository, EmployeeRepository employeeRepository, 
			PasswordEncoder passwordEncoder) {
		Organization lastOrganization = null;
		Organization firstOrganization = null;

		for (int i = 0; i < 10000; i++) {
			Organization organization = new Organization();
			organization.setRegistryNumber("xxxxxxxxxxxxxxxxxxx" + i);
			organization.setName("Тестовая организация" + i);	
			organization.setInn("123456" + i);
			organization.setOktmo("787878" + i);
			organization.setOgrn("676767" + i);
			organization.setLocked(i % 2 == 0);
			
			Employee employee = new Employee();
			employee.setFirstName(getRandom(FIRST_NAME));
			employee.setLastName(getRandom(LAST_NAME));
			employee.setPatronymic("Иванович" + i);
			employee.setBirthDate(new Date(new Date().getTime() - i*1000*60*60*24));
			employee.setLogin("i.ivanov" + i);
			employee.setSnils("123123" + i);
			employee.setPasswordHash(passwordEncoder.encode("i.ivanov" + i));
			
			if (!organization.isLocked()) {
				organization.setCreator(employee);
				employee.setOrganization(lastOrganization);
				employee.getRoles().add(Role.Declarant);
				organization.setParent(lastOrganization);
			}else {
				if (firstOrganization != null) {
					organization.setCreator(employee);
					employee.getRoles().add(Role.Declarant);
					employee.getRoles().add(Role.Executor);
					employee.setOrganization(firstOrganization);
					organization.setParent(firstOrganization);					
				}else {
					Organization org = new Organization();
					org.setId(1l);
					employee.setOrganization(org);
				}
			}
			organization.setShortName("Краткое наименование" + i);
			organization.setPaddr("Почтовый адрес" + i);
			organization.setJaddr("Юридический адрес" + i);
			organization.setKbk("99090" + i);
			organization.setKpp("090909" + i);
			organization.setUrn("1" + i);
			
			organization.setBankName("Банк №" + i);
			organization.setRs("123456789012356" + i);
			organization.setLs("123456789065432" + i);
			organization.setKs("654321789065432" + i);
			organization.setBik("55" + i);
			
			organization.setTel("893749" + i);
			organization.setEmail("89" + i + "@mail.ru");
			
			employeeRepository.save(employee);
			organizationRepository.save(organization);
			lastOrganization = organization;
			if (i == 0) {
				firstOrganization = organization;
			}
		}
		
	}

	private void createEmployees(EmployeeRepository employeeRepository, PasswordEncoder passwordEncoder){
		for (int i = 0; i < 1000; i++) {
			Employee employee = new Employee();
			employee.setLogin("login" + i);
			employee.setPasswordHash(passwordEncoder.encode("login" + i));
			employee.setLastName(getRandom(LAST_NAME));
			employee.setFirstName(getRandom(FIRST_NAME));
			employee.setPatronymic("Степанович" + i);
			employee.setInn("123456" + i);
			employee.setSnils("123456" + i);

			Organization organization = new Organization();
			organization.setId(new Long(i+1));
			organization.setRegistryNumber("xxxxxxxxxxxxxxxxxxx" + i);
			organization.setName("Тестовая организация" + i);
			organization.setInn("123456" + i);
			organization.setOktmo("787878" + i);
			organization.setOgrn("676767" + i);

			employee.setOrganization(organization);
			employee.setBirthDate(new Date(new Date().getTime() - i*1000*60*60*24));
			employee.setLocked(i % 2 == 0);
			employeeRepository.save(employee);
		}
	}

	private void createOperator(OrganizationRepository organizationRepository, EmployeeRepository employeeRepository,
			PasswordEncoder passwordEncoder) {
		Organization organization = organizationRepository.findById(1l).orElse(null);
		if (organization == null) {
			organization = new Organization();
			organization.setName("Демонстрационная организация");
			organization.setInn("123456");
			organization.setOktmo("787878");
			organization.setOgrn("676767");
			
			organization.setTel("89374094041");
			organization.setEmail("test@mail.ru");
			organizationRepository.save(organization);
			getLogger().info("Create new demo organization");
		} else {
			getLogger().info("Use existing demo organization");
		}
		
		Employee employee = employeeRepository.findByLogin("demooperator");
		if (employee == null) {
			employee = new Employee();
			employee.setLogin("demooperator");
			employee.setPasswordHash(passwordEncoder.encode("demooperator"));
			employee.setLastName("Демонстрационный");
			employee.setFirstName("Оператор");
			employee.setPatronymic("Алексеевич");
			employee.setInn("123456");
			employee.setSnils("123456");
			employee.setBirthDate(new Date());

			employee.getRoles().add(Role.Operator);
			employee.setOrganization(organization);
			employeeRepository.save(employee);
			getLogger().info("Create new demo operator");
		} else {
			getLogger().info("Use existing demo operator");
		}
		
		Employee employee2 = employeeRepository.findByLogin("demoexecutor");
		if (employee2 == null) {
			employee2 = new Employee();
			employee2.setLogin("demoexecutor");
			employee2.setPasswordHash(passwordEncoder.encode("demoexecutor"));
			employee2.setLastName("Иванов");
			employee2.setFirstName("Иван");
			employee2.setPatronymic("Алексеевич");
			employee2.setInn("123455");
			employee2.setSnils("123455");
			employee2.setBirthDate(new Date());

			employee2.getRoles().add(Role.Executor);
			employee2.setOrganization(organization);
			employeeRepository.save(employee2);
			getLogger().info("Create new demo operator");
		} else {
			getLogger().info("Use existing demo operator");
		}
	}
	
	private void createProjects(ProjectRepository projectRepository) {
		Project project = new Project();
		project.setId(1l);
		project.setRating(45678l);
		project.setName("Безопасный пешеходный переход");
		project.setCost(66746l);
		project.setDescription("Необходимо сделать безопасных пешеходный переход на соединение дорог по ул. Большая Арбековская и 5-ый проезд Кольцова.");
		project.setTarget("Обеспечение безопасности граждан");
		project.setTask("");
		project.setInitiator("Кандрашкин Алексей Владимирович");
		project.setMunitipality_level("Муниципальный");
		project.setDefstatus(DefinitionStatus.Expert);	
		projectRepository.save(project);
		
		Project project2 = new Project();
		project2.setId(2l);
		project2.setRating(46678l);
		project2.setName("Понтонный мост");
		project2.setCost(2567340l);
		project2.setDescription("Обращаемся к Вам по поводу проезда по понтонному мосту, связывающему район Барковки и район Терновки");
		project2.setTarget("Удобство граждан");
		project2.setTask("");
		project2.setInitiator("Кандрашкин Алексей Владимирович");
		project2.setMunitipality_level("Муниципальный");
		project2.setDefstatus(DefinitionStatus.Expert);	
		projectRepository.save(project2);
		
		Project project3 = new Project();
		project3.setId(3l);
		project3.setRating(43678l);
		project3.setName("Сделать ливневую канализацию на ул. Мира");
		project3.setCost(1434840l);
		project3.setDescription("На двух регулируемых пешеходных переходах на ул. Мира в районе домов № 2 и № 6 во время дождя по проезжей части вдоль бордюра течет настоящая река. Ширина потока порядка 1,5 м.");
		project3.setTarget("Решение проблемы");
		project3.setTask("");
		project3.setInitiator("Кандрашкин Алексей Владимирович");
		project3.setMunitipality_level("Муниципальный");
		project3.setDefstatus(DefinitionStatus.Expert);	
		projectRepository.save(project3);
		
		Project project4 = new Project();
		project4.setId(4l);
		project4.setRating(69678l);
		project4.setName("Детская площадка на ул. Мира");
		project4.setCost(4737800l);
		project4.setDescription("Обращаеимся по поводу ремонта детской площадки");
		project4.setTarget("Решение проблемы");
		project4.setTask("");
		project4.setInitiator("Шиляпов Максим Сергеевич");
		project4.setMunitipality_level("Муниципальный");
		project4.setDefstatus(DefinitionStatus.Expert);	
		projectRepository.save(project4);
		
	}

	
}
