package ru.gbu.sr.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.vaadin.spring.events.annotation.EnableEventBus;

import ru.gbu.sr.app.security.SecurityConfig;
import ru.gbu.sr.backend.EmployeeRepository;
import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.backend.service.EmployeeService;
import ru.gbu.sr.backend.util.LocalDateJpaConverter;
import ru.gbu.sr.backend.util.LocalDateTimeJpaConverter;
import ru.gbu.sr.ui.AppUI;

@SpringBootApplication(scanBasePackageClasses = { AppUI.class, Application.class, EmployeeService.class,
		SecurityConfig.class})
@EnableJpaRepositories(basePackageClasses = {EmployeeRepository.class})
@EntityScan(basePackageClasses = { Employee.class, LocalDateJpaConverter.class, LocalDateTimeJpaConverter.class })
@EnableEventBus
@EnableJpaAuditing
public class Application extends SpringBootServletInitializer {

	public static final String APP_URL = "/";
	public static final String LOGIN_URL = "/login.html";
	public static final String LOGOUT_URL = "/login.html?logout";
	public static final String LOGIN_FAILURE_URL = "/login.html?error";
	public static final String LOGIN_PROCESSING_URL = "/login";

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}
}
