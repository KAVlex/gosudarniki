package ru.gbu.sr.app;

import com.vaadin.server.CustomizedSystemMessages;
import com.vaadin.server.SystemMessages;
import com.vaadin.server.SystemMessagesInfo;
import com.vaadin.server.SystemMessagesProvider;

public class SystemMessagesProviderImpl implements SystemMessagesProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public SystemMessages getSystemMessages(SystemMessagesInfo systemMessagesInfo) {
		 CustomizedSystemMessages messages = new CustomizedSystemMessages();
	    messages.setSessionExpiredCaption("Сессия окончена");
	    messages.setSessionExpiredMessage("Обратите внимание на любые несохраненные данные, "
	    		+ "и <U> нажмите здесь </U> или нажмите клавишу ESC, чтобы продолжить.");
	    
	    messages.setCommunicationErrorCaption("Проблема соединения");
	    messages.setCommunicationErrorMessage("Обратите внимание на любые несохраненные данные,"
	    		+ " и <U> нажмите здесь </U> или нажмите клавишу ESC, чтобы продолжить.");
	    
	    messages.setAuthenticationErrorCaption("Проблема аутентификации");
	    messages.setAuthenticationErrorMessage("Обратите внимание на любые несохраненные данные,"
	    		+ " и <U> нажмите здесь </U> или нажмите клавишу ESC, чтобы продолжить.");
	    
	    messages.setInternalErrorCaption("Внутренняя ошибка");
	    messages.setInternalErrorMessage("Пожалуйста, сообщите об этом администратору."
	    		+ " <br/> Обратите внимание на любые несохраненные данные,"
	    		+ " и <и> нажмите здесь </U> или нажмите клавишу ESC, чтобы продолжить.");
	    
	    messages.setCookiesDisabledCaption("Cookies отключены");
	    messages.setCookiesDisabledMessage("Приложение требует cookies для работы функции. <br/> Пожалуйста, включите cookies в вашем браузере"
	    		+ " и <и> нажмите здесь </U> или нажмите клавишу ESC, чтобы повторить попытку.");
		return messages;
	}

}
