package ru.gbu.sr.app.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.backend.service.EmployeeService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private final EmployeeService employeeService;

	@Autowired
	public UserDetailsServiceImpl(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Employee employee = employeeService.findByLogin(username);
		if (null == employee) {
			throw new UsernameNotFoundException("Нет пользователя с логином: " + username);
		} else if (employee.isLocked()){
			throw new UsernameNotFoundException("Пользователь " + username + " заблокирован");
		} else {
			List<SimpleGrantedAuthority> roles = new ArrayList<SimpleGrantedAuthority>();
			employee.getRoles().forEach(role -> roles.add(new SimpleGrantedAuthority(role.name())));
			return new org.springframework.security.core.userdetails.User(employee.getLogin(), employee.getPasswordHash(), roles);
		}
	}
}