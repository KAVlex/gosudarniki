package ru.gbu.sr.ui;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.vaadin.spring.access.SecuredViewAccessControl;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.navigator.ViewLeaveAction;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.spring.annotation.UIScope;

import ru.gbu.sr.app.security.SecurityUtils;
import ru.gbu.sr.backend.data.entity.Service;
import ru.gbu.sr.ui.navigation.NavigationManager;
import ru.gbu.sr.ui.views.mentions.MentionsView;
import ru.gbu.sr.ui.views.organizations.OrganizationsView;
import ru.gbu.sr.ui.views.projects.ProjectsView;
import ru.gbu.sr.ui.views.services.ServicesView;
import ru.gbu.sr.ui.views.users.UsersView;

import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.MenuBar.MenuItem;


@SpringViewDisplay
@UIScope
public class MainView extends MainViewDesign implements ViewDisplay {

	private static final long serialVersionUID = 1L;
	private final Map<String, View> navigationCaсhe = new HashMap<>();
	private final NavigationManager navigationManager;
	private final SecuredViewAccessControl viewAccessControl;

	@Autowired
	public MainView(NavigationManager navigationManager, SecuredViewAccessControl viewAccessControl) {
		this.navigationManager = navigationManager;
		this.viewAccessControl = viewAccessControl;
	}

	@PostConstruct
	public void init() {
		MenuItem user = topMenu.addItem(SecurityUtils.getUsername(), null, null);
		user.addItem("Выход", new MenuBar.Command() {
			private static final long serialVersionUID = 1L;

			@Override
			public void menuSelected(MenuItem selectedItem) {
				logout();
			}
		});		
		
		attachNavigation("Организации", OrganizationsView.class);
		attachNavigation("Пользователи", UsersView.class);
		attachNavigation("Упоминания", MentionsView.class);
		attachNavigation("Отзывы", MentionsView.class);
		attachNavigation("Петиции", MentionsView.class);
		attachNavigation("Инициативное бюджетирование", ProjectsView.class);
		attachNavigation("Опросы", MentionsView.class);
		attachNavigation("Собрания", ServicesView.class);
		
		logout.addClickListener(e -> logout());
	}

	private void attachNavigation(String name, Class<? extends View> targetView) {
		boolean hasAccessToView = viewAccessControl.isAccessGranted(targetView);

		if (hasAccessToView) {
			topMenu.addItem(name, new MenuBar.Command() {
				
				private static final long serialVersionUID = 1L;

				@Override
				public void menuSelected(MenuItem selectedItem) {
					navigationManager.navigateTo(targetView);
				}
			});
		}
	}
	
	private void attachNavigation(MenuBar.MenuItem menuItem, String name, Class<? extends View> targetView) {
		boolean hasAccessToView = viewAccessControl.isAccessGranted(targetView);

		if (hasAccessToView) {
			menuItem.addItem(name, new MenuBar.Command() {
				
				private static final long serialVersionUID = 1L;

				@Override
				public void menuSelected(MenuItem selectedItem) {
					navigationManager.navigateTo(targetView);
				}
			});
		}
	}

	@Override
	public void showView(View view) {
		content.removeAllComponents();
		View cache = navigationCaсhe.get(navigationManager.getState());
		if (cache != null && cache instanceof Cacheable) {
			view = cache;
		}else {
			navigationCaсhe.put(navigationManager.getState(), view);			
		}
		content.addComponent(view.getViewComponent());
	}

	public void logout() {
		ViewLeaveAction doLogout = () -> {
			UI ui = getUI();
			ui.getSession().getSession().invalidate();
			ui.getPage().reload();
		};

		navigationManager.runAfterLeaveConfirmation(doLogout);
	}

}
