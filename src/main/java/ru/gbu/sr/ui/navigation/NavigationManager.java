package ru.gbu.sr.ui.navigation;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.internal.Conventions;
import com.vaadin.spring.navigator.SpringNavigator;

import ru.gbu.sr.ui.views.Mode;
import ru.gbu.sr.ui.views.organizations.OrganizationsView;

/**
 * Governs view navigation of the app.
 */
@Component
@UIScope
public class NavigationManager extends SpringNavigator {

	private static final long serialVersionUID = 1L;
	private static final String MODE = "mode";
	private static final String SEPARATOR = "&";
	
	/**
	 * Find the view id (URI fragment) used for a given view class.
	 *
	 * @param viewClass
	 *            the view class to find the id for
	 * @return the URI fragment for the view
	 */
	public String getViewId(Class<? extends View> viewClass) {
		SpringView springView = viewClass.getAnnotation(SpringView.class);
		if (springView == null) {
			throw new IllegalArgumentException("The target class must be a @SpringView");
		}

		return Conventions.deriveMappingForView(viewClass, springView);
	}
	
	@Override
    protected boolean beforeViewChange(ViewChangeEvent event) {
		//String uri = "#!" + event.getViewName() + "/" + event.getParameters();
		//ExternalResource er = new ExternalResource(uri);
		//breadCrums.addLink(new Link(event.getViewName(), er));
        return fireBeforeViewChange(event);
    }

	/**
	 * Navigate to the given view class.
	 *
	 * @param viewClass
	 *            the class of the target view, must be annotated using
	 *            {@link SpringView @SpringView}
	 */
	public void navigateTo(Class<? extends View> targetView) {
		String viewId = getViewId(targetView);
		navigateTo(viewId);
	}

	public void navigateTo(Class<? extends View> targetView, Object parameter) {
		String viewId = getViewId(targetView);
		navigateTo(viewId + "/" + parameter.toString());
	}
	
	public void navigateTo(Class<? extends View> targetView, Object parameter, Mode mode) {
		String viewId = getViewId(targetView);
		navigateTo(viewId + "/" + parameter.toString() + SEPARATOR + MODE + "=" + mode.name().toLowerCase());
	}
	
	public String getParameter(String uri) {
		if (uri != null) {
			return uri.contains(SEPARATOR) ? uri.substring(0, uri.indexOf(SEPARATOR)) : uri;	
		}
		return "";
	}
	
	public Mode getMode() {
		Mode mode = Mode.VIEW;
		Map<String, String> mapParameter = getStateParameterMap();
		if (mapParameter != null && mapParameter.get(MODE) != null) {
			try {
				mode = Mode.valueOf(mapParameter.get(MODE).toUpperCase());
			} catch (Exception e) {
				return null;
			}			
		}
		return mode;
	}

	public void navigateToDefaultView() {
		if (!getState().isEmpty()) {
			return;
		}
		navigateTo(OrganizationsView.class);
	}

	/**
	 * Update the parameter of the the current view without firing any
	 * navigation events.
	 *
	 * @param parameter
	 *            the new parameter to set, never <code>null</code>,
	 *            <code>""</code> to not use any parameter
	 */
	public void updateViewParameter(String parameter) {
		String viewName = getViewId(getCurrentView().getClass());
		String parameters;
		if (parameter == null) {
			parameters = "";
		} else {
			parameters = parameter;
		}

		updateNavigationState(new ViewChangeEvent(this, getCurrentView(), getCurrentView(), viewName, parameters));
	}

	public void jsBack() {
		Page.getCurrent().getJavaScript().execute("window.history.back()");		
	}
}
