package ru.gbu.sr.ui;

import java.util.ArrayList;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;

public class Breadcrumbs {

	private ArrayList<Link> crums;
	private Panel breadCrums;
	private HorizontalLayout hl;
	
	public Breadcrumbs(Panel breadCrums) {
		super();
		this.crums = new ArrayList<Link>();
		this.breadCrums = breadCrums;
		this.hl = (HorizontalLayout)breadCrums.getContent();
	}
	
	public int getCrumsCount(){
		return crums.size();
	}

	public ArrayList<Link> getCrums() {
		return crums;
	}

	public Panel getBreadCrums() {
		return breadCrums;
	}

	public HorizontalLayout getHl() {
		return hl;
	}
	
	public void addLink(Link link){
		if (hl.getComponentCount() >= 1){
			hl.getComponent(hl.getComponentCount()-1).setEnabled(true);
		}
		link.setEnabled(false);
		hl.addComponent(link);
		crums.add(link);
	}
	
	public void removeLinks(String num){
		Integer page = null;
		try{
			page = Integer.parseInt(num);
			Link link = null;
			int size = crums.size(); 
			for(int i=size;i>page + 1 ;i--){
				link = crums.remove(i - 1);
				hl.removeComponent(link);
			}
			if (hl.getComponentCount() >= 1){
				hl.getComponent(hl.getComponentCount()-1).setEnabled(false);
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	

	
	public void updateStartLink(){
		if(crums.size()>1){
			Link link =  crums.get(0);
			ExternalResource er = (ExternalResource)link.getResource();
			String resource =  er.getURL();
			if(resource.startsWith("/"))resource = resource.substring(1);
			resource = resource.replace("#!workArea", "");
			if(resource.startsWith("/"))resource = resource.substring(1);
			
			er = new ExternalResource(resource);
			link.setResource(er);
		}
	}
	
	public Link getBackLink(){
		return crums.get(crums.size() - 2);
	}
	
	public void clearCrums(){
		crums.clear();
	};
}
