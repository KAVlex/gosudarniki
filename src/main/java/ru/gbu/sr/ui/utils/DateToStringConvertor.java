package ru.gbu.sr.ui.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

public class DateToStringConvertor implements Converter<String, Date> {

	private static final long serialVersionUID = 1L;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	@Override
	public Result<Date> convertToModel(String value, ValueContext context) {
		if (value != null && !value.isEmpty()) {
			try {
				Date date = sdf.parse(value);
				return Result.ok(date);
			} catch (ParseException e) {
				e.printStackTrace();
				return Result.error(e.getMessage());
			}			
		}
		return Result.ok(null);
	}

	@Override
	public String convertToPresentation(Date value, ValueContext context) {
		return value != null ? sdf.format(value) : "";
	}
	
	public static String format(Date value) {
		return value != null ? sdf.format(value) : "";
	}

}
