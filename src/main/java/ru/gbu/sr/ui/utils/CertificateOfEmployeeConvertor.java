package ru.gbu.sr.ui.utils;

import java.security.cert.X509Certificate;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

import ru.codeinside.gses.cert.NameParts;
import ru.codeinside.gses.cert.X509;
import ru.gbu.sr.backend.data.entity.CertificateOfEmployee;

public class CertificateOfEmployeeConvertor implements Converter<String, CertificateOfEmployee> {

	private static final long serialVersionUID = 1L;

	private CertificateOfEmployee certificateOfEmployee;
	boolean certificateWasRemoved = false;

	@Override
	public String convertToPresentation(CertificateOfEmployee certificate, ValueContext context) {
		certificateOfEmployee = certificate;
		if (certificate != null && certificate.getX509() != null) {
			X509Certificate x509Certificate = X509.decode(certificate.getX509());
			NameParts subjectParts = X509.getSubjectParts(x509Certificate);
			return subjectParts.getShortName();
		}
		return "";
	}

	@Override
	public Result<CertificateOfEmployee> convertToModel(String value, ValueContext context) {
		return Result.ok(certificateWasRemoved ? null : certificateOfEmployee);
	}

	public void setCertificateWasRemoved(boolean certificateWasRemoved) {
		this.certificateWasRemoved = certificateWasRemoved;
	}

	public void setCertificateOfEmployee(CertificateOfEmployee certificateOfEmployee) {
		this.certificateOfEmployee = certificateOfEmployee;
	}

}
