package ru.gbu.sr.ui.utils;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

public class LongToStringConvertor implements Converter<String, Long>{

	private static final long serialVersionUID = 1L;

	@Override
	public Result<Long> convertToModel(String value, ValueContext context) {
		if (value == null || value.isEmpty())
			return Result.ok(null);
		try {
			return Result.ok(Long.valueOf(value));	
		} catch (Exception e) {
			return Result.error(e.getMessage());
		}
	}

	@Override
	public String convertToPresentation(Long value, ValueContext context) {
		return value != null ? value.toString() : "";
	}

}
