package ru.gbu.sr.ui.utils;

import java.security.SecureRandom;
import java.util.Random;

import ru.codeinside.gses.webui.utils.Translit;
import ru.gbu.sr.backend.service.EmployeeService;
import ru.gbu.sr.backend.service.UserFriendlyDataException;

public class Generator {
	
    static char[] SYMBOLS = (new String("(%*)?@#$~")).toCharArray();
    static char[] LOWERCASE = (new String("abcdefghijklmnopqrstuvwxyz")).toCharArray();
    static char[] UPPERCASE = (new String("ABCDEFGHIJKLMNOPQRSTUVWXYZ")).toCharArray();
    static char[] NUMBERS = (new String("0123456789")).toCharArray();
    static char[] ALL_CHARS = (new String("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789^$*.[]{}()?-\"!@#%&/\\,><':;|_~`")).toCharArray();
    static final int DEFAULT_PASS_LENGTH = 8;
    static Random rand = new SecureRandom();

	/**
	 * Условие: Логин должен задаваться латинскими буквами и состоять из данных
	 * следующих полей: Маска в начале (1ая заглавная буква имени, 1ая заглавная
	 * буква отчества, текущего пользователя). Спец.символ - . (точка). Маска в
	 * конце (Фамилия текущего пользователя). Пример логина: II.Ivanov Проверка: в
	 * случае наличия в БД, аналогичного логина, система должна прибавлять единицу к
	 * текущему логину [L = current login + 1] (II.Ivanov1, II.Ivanov2 и т.д.).
	 * 
	 * @param fio
	 * @return
	 */
	public static String generateLogin(String surname, String name, String patronymic, EmployeeService employeeService) {
		if (surname == null || surname.trim().isEmpty() || name == null || name.trim().isEmpty()) {
			throw new UserFriendlyDataException("Фамилия и/или Имя не заданы, сгенерировать пароль невозможно");
		}
		String textForLogin = name.trim().substring(0, 1).toUpperCase();
		if (patronymic != null && !patronymic.trim().isEmpty()) {
			textForLogin += patronymic.trim().substring(0, 1).toUpperCase();
		}
		surname = surname.trim();
	    String s1 = surname.substring(0, 1).toUpperCase();
	    String surnameCapitalized = s1 + surname.substring(1).toLowerCase();
		textForLogin += ".".concat(surnameCapitalized);
		String login = Translit.toTranslit(textForLogin);
		int i = 1;
		while (employeeService.existLogin(login)) {
			login = Translit.toTranslit(textForLogin + i++);
		}
		return login;
	}
	
	/**
	 * Условия: Длина – 8 символов Вхождения символов: спец. символы (%, *, ),?, @,
	 * #, $, ~), цифры (0-9), буквы (строчные, прописные).
	 * 
	 * @return
	 */
	public static String generatePassword(int length) {
        assert length >= 4;
        char[] password = new char[length];

        //get the requirements out of the way
        password[0] = LOWERCASE[rand.nextInt(LOWERCASE.length)];
        password[1] = UPPERCASE[rand.nextInt(UPPERCASE.length)];
        password[2] = NUMBERS[rand.nextInt(NUMBERS.length)];
        password[3] = SYMBOLS[rand.nextInt(SYMBOLS.length)];

        //populate rest of the password with random chars
        for (int i = 4; i < length; i++) {
            password[i] = ALL_CHARS[rand.nextInt(ALL_CHARS.length)];
        }

        //shuffle it up
        for (int i = 0; i < password.length; i++) {
            int randomPosition = rand.nextInt(password.length);
            char temp = password[i];
            password[i] = password[randomPosition];
            password[randomPosition] = temp;
        }

        return new String(password);
	}
	
	public static String generatePassword() {
		return generatePassword(DEFAULT_PASS_LENGTH);
	}
	
}
