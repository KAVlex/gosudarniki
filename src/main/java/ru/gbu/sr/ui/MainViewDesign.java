package ru.gbu.sr.ui;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class MainViewDesign extends VerticalLayout {
	protected Label activeViewName;
	protected MenuBar topMenu;
	protected Panel breadcrumbs;
	protected Button logout;
	protected VerticalLayout content;

	public MainViewDesign() {
		setStyleName("main-screen");
		content = new VerticalLayout();
        addComponent(buildTopMenu());
        addComponent(buildBreadcrumbs());
        addComponent(content);
        activeViewName = new Label("Тест");
        logout = new Button();
        setMargin(false);
		//Design.read(this);
	}
	
	private MenuBar buildTopMenu(){
		topMenu = new MenuBar();
		topMenu.setWidth("100%");
		topMenu.addStyleName("topMenu");
		return topMenu;
	}
	
	private Panel buildBreadcrumbs() {
		breadcrumbs = new Panel();
		breadcrumbs.setWidth("100.0%");
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setWidth("100.0%");
		horizontalLayout.setHeight("100.0%");
		horizontalLayout.setMargin(false);
		breadcrumbs.setContent(horizontalLayout);
		
		return breadcrumbs;
	}
}
