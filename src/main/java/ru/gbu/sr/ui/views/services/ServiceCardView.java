package ru.gbu.sr.ui.views.services;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder.BindingBuilder;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.navigator.ViewBeforeLeaveEvent;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Label;

import ru.gbu.sr.backend.data.entity.Service;
import ru.gbu.sr.ui.utils.DateToStringConvertor;
import ru.gbu.sr.ui.views.AbstractCardView;
import ru.gbu.sr.ui.views.Mode;

@SpringView
@Secured({"Operator", "Executor"})
public class ServiceCardView extends ServiceCardViewDesign implements AbstractCardView{

	private static final long serialVersionUID = 1L;
	private final ServiceCardPresenter presenter;
	private BeanValidationBinder<Service> binder;
	
	@Autowired
	public ServiceCardView(ServiceCardPresenter presenter) {
		this.presenter = presenter;
	}
	
	@PostConstruct
	public void init() {
		presenter.init(this);
		binder = new BeanValidationBinder<>(Service.class);
		binder.forField(updater).bind(service -> { 
			return (service.getUpdater() != null) ? service.getUpdater().getLogin() : "";
		}, null);
		binder.forField(dateUpdated).withConverter(new DateToStringConvertor()).bind("dateUpdated");
		addValidators();
		binder.bindInstanceFields(this);
		binder.addValueChangeListener(e -> presenter.hasChanges());
		
		cancel.addClickListener(e -> presenter.cancelPressed());
		save.addClickListener(e -> presenter.savePressed());
		saveExit.addClickListener(e -> presenter.saveExitPressed());
		edit.addClickListener(e -> presenter.editPressed());
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		presenter.enter(event);
	}
	
	private void addValidators(){
		addValidator(name, "Значение поля 'Наименование' должно содержать не менее 2 символов", "^\\S+[\\S\\s]*\\S+$");
	}
	
	public void showNotFound() {
		removeAllComponents();
		addComponent(new Label("Процедура не найдена"));
	}
	
	public void setService(Service service) {
		binder.setBean(service);
		presenter.setNoChanges();
	}
	
	public void setMode(Mode mode) {
		binder.setReadOnly(mode.equals(Mode.VIEW));
		dateUpdated.setReadOnly(true);
		edit.setVisible(mode.equals(Mode.VIEW));
		saveExit.setVisible(!mode.equals(Mode.VIEW));
		save.setVisible(!mode.equals(Mode.VIEW));
		switch (mode) {
			case ADD:
				modeLabel.setValue("Создание собрания");
				break;
			case EDIT:
				modeLabel.setValue("Редактирование собрания");
				break;
			default:
				modeLabel.setValue("Просмотр собрания");
				break;
		}
	}
	
	public BeanValidationBinder<Service> getBinder(){
		return binder;
	}	
	
	@Override
	public void beforeLeave(ViewBeforeLeaveEvent event) {
		presenter.beforeLeave(event);
	}
	
	public Service getService() {
		return binder.getBean();
	}
	
	public BindingBuilder<Service, String> addValidator(AbstractTextField field, String text, String regexp) {
		return binder.forMemberField(field).withValidator(new RegexpValidator(text, regexp));
	}
	
}
