package ru.gbu.sr.ui.views.projects;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;

import ru.gbu.sr.backend.data.ProjectFind;
import ru.gbu.sr.backend.data.entity.Project;
import ru.gbu.sr.backend.service.ProjectService;
import ru.gbu.sr.ui.components.Pagination;

@SpringComponent
@PrototypeScope
public class ProjectsDataProvider extends PageableDataProvider<Project, Object> {

	private static final long serialVersionUID = 1L;

	@Autowired
	ProjectService serviceService;

	private ProjectFind serviceFind = new ProjectFind();
	private Pageable pageable = PageRequest.of(0, Pagination.DEFAULT_LIMIT);

	@Override
	protected Page<Project> fetchFromBackEnd(Query<Project, Object> query, Pageable pageable) {
		pageable = PageRequest.of(this.pageable.getPageNumber(), this.pageable.getPageSize(), pageable.getSort());
		return serviceService.findAnyMatching(serviceFind, pageable);
	}

	@Override
	protected int sizeInBackEnd(Query<Project, Object> query) {
		int size = serviceService.findAnyMatching(serviceFind, pageable).getNumberOfElements();
		return size;
	}

	@Override
	protected List<QuerySortOrder> getDefaultSortOrders() {
		List<QuerySortOrder> sortOrders = new ArrayList<>();
		sortOrders.add(new QuerySortOrder("id", SortDirection.ASCENDING));
		return sortOrders;
	}

	public int filter(ProjectFind serviceFind, Pageable pageable) {
		this.serviceFind = serviceFind;
		this.pageable = pageable;
		refreshAll();
		return (int) serviceService.countAnyMatching(serviceFind);
	}

}