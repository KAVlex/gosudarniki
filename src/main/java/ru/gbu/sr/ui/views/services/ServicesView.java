package ru.gbu.sr.ui.views.services;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.themes.ValoTheme;

import ru.gbu.sr.backend.data.entity.Service;
import ru.gbu.sr.ui.Cacheable;
import ru.gbu.sr.ui.components.Pagination;
import ru.gbu.sr.ui.views.Mode;

@SpringView
@Secured({"Operator", "Executor"})
public class ServicesView extends ServicesViewDesign implements View, Cacheable{
	
	private static final long serialVersionUID = 1L;
	
	private final ServicesPresenter presenter;
	
	@Autowired
	public ServicesView(ServicesPresenter presenter) {
		this.presenter = presenter;
	}
	
	@PostConstruct
	public void init() {
		presenter.init(this);
		servicesGrid.setColumns("id", "name", "dateCreated");
		
		//servicesGrid.getColumn("id").setCaption("Код").setResizable(true).setHidable(true).setExpandRatio(1);
		//servicesGrid.getColumn("registerCode").setCaption("Реестровый номер").setResizable(true).setHidable(true).setExpandRatio(1);
		servicesGrid.getColumn("name").setCaption("Наименование").setResizable(true).setExpandRatio(1).
			setDescriptionGenerator(Service::getName);
		servicesGrid.getColumn("dateCreated").setCaption("Дата создания").setResizable(true).setHidable(true).setExpandRatio(1);
		servicesGrid.addComponentColumn(this::buildButtons).setExpandRatio(2);
		
		ok.addClickListener(e -> {
			pagination.firePagedChangedEvent();
		});
		cancel.addClickListener(e -> {
			presenter.getBinder().readBean(null);
			pagination.firePagedChangedEvent();
		});
		add.addClickListener(e -> presenter.newService());
	}

	public Grid<Service> getGrid(){
		return servicesGrid;
	}
	
	public Pagination getPagination() {
		return pagination;
	}
	
	private HorizontalLayout buildButtons(Service service) {
    	HorizontalLayout horizontalLayout = new HorizontalLayout();
    	Button view = createButton(VaadinIcons.EYE, "Просмотр", service.getId());
    	Button edit = createButton(VaadinIcons.PENCIL, "Редактирование", service.getId());
    	
    	horizontalLayout.addComponent(view);
    	horizontalLayout.addComponent(edit);
       
    	view.addClickListener(e -> presenter.openServiceCard((Long) e.getButton().getData(), Mode.VIEW));
    	edit.addClickListener(e -> presenter.openServiceCard((Long) e.getButton().getData(), Mode.EDIT));
        return horizontalLayout;
    }
	
	private static Button createButton(VaadinIcons icon, String description, Long id) {
    	Button button = new Button(icon);
    	button.addStyleName(ValoTheme.BUTTON_TINY);
    	button.setDescription(description);
    	button.setData(id);
    	return button;
	}
}
