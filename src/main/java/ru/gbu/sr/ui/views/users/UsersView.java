package ru.gbu.sr.ui.views.users;

import javax.annotation.PostConstruct;

import com.vaadin.data.ValueProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.renderers.ComponentRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.addons.ComboBoxMultiselect;

import ru.gbu.sr.app.security.SecurityUtils;
import ru.gbu.sr.backend.data.Role;
import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.backend.data.entity.Organization;
import ru.gbu.sr.ui.Cacheable;
import ru.gbu.sr.ui.components.Pagination;
import ru.gbu.sr.ui.navigation.NavigationManager;
import ru.gbu.sr.ui.views.Mode;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

@SpringView
@Secured({"Operator"})
public class UsersView extends UsersViewDesign implements View, Cacheable {

	private static final long serialVersionUID = 1L;
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

	private final UsersPresenter presenter;
	private final NavigationManager navigationManager;

	@Autowired
	public UsersView(UsersPresenter presenter, NavigationManager navigationManager) {
		this.presenter = presenter;
		this.navigationManager = navigationManager;
	}

	@PostConstruct
	public void init() {
		presenter.init(this);
		
		online.setVisible(false);
		roles.setItemCaptionGenerator(Role::getDescription);
		roles.setItems(Role.values());
		
		dateStart.setDateFormat("dd/MM/yyyy");
		dateEnd.setDateFormat("dd/MM/yyyy");
		dateStart.setLocale(new Locale("ru", "RU"));
		dateEnd.setLocale(new Locale("ru", "RU"));
		
		userGrid.setColumns("locked", "lastName", "firstName", "patronymic", "organization", "snils", "birthDate", "roles");

		@SuppressWarnings("unchecked")
		Column<Employee, Boolean> lockedColumn = (Column<Employee, Boolean>) userGrid.getColumn("locked");
		lockedColumn.setStyleGenerator(item -> "v-align-center");
		lockedColumn.setRenderer(bool -> buildLockedLabel(bool), new ComponentRenderer());

		@SuppressWarnings("unchecked")
		Grid.Column<Employee, Date> dateColumn = (Grid.Column<Employee, Date>) userGrid.getColumn("birthDate");
		dateColumn.setStyleGenerator(item -> "v-align-center");
		dateColumn.setRenderer(date -> date != null ? DATE_FORMAT.format(date) : "", new TextRenderer());

		@SuppressWarnings("unchecked")
		Grid.Column<Employee, Set<Role>> rolesColumn = (Grid.Column<Employee, Set<Role>>) userGrid.getColumn("roles");
		rolesColumn.setStyleGenerator(item -> "v-align-center");
		rolesColumn.setRenderer(new ValueProvider<Set<Role>, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(Set<Role> source) {
				if(source != null && source.size() > 0){
					StringBuilder roles = new StringBuilder();
					int size = source.size();
					for(Role role : source){
						size--;
						roles.append(role.description);
						if(size > 0) {
							roles.append("; ");
						}
					}
					return roles.toString().trim();
				}
				return "";
			}
		}, new TextRenderer());

		@SuppressWarnings("unchecked")
		Grid.Column<Employee, Organization> organizationColumn = (Grid.Column<Employee, Organization>) userGrid.getColumn("organization");
		organizationColumn.setStyleGenerator(item -> "v-align-center");
		organizationColumn.setRenderer(org -> org != null ? org.getName() : "", new TextRenderer());

		lockedColumn.setCaption("").setResizable(true).setHidable(true).setExpandRatio(0);
		userGrid.getColumn("lastName").setCaption("Фамилия").setResizable(true).setExpandRatio(1);
		userGrid.getColumn("firstName").setCaption("Имя").setResizable(true).setHidable(true).setExpandRatio(1);
		userGrid.getColumn("patronymic").setCaption("Отчество").setResizable(true).setHidable(true).setExpandRatio(1);
		organizationColumn.setCaption("Организация").setResizable(true).setHidable(true).setExpandRatio(1);
		userGrid.getColumn("snils").setCaption("СНИЛС").setResizable(true).setHidable(true).setExpandRatio(1);
		dateColumn.setCaption("Дата рождения").setResizable(true).setHidable(true).setExpandRatio(1);
		rolesColumn.setCaption("Роли").setResizable(true).setHidable(true).setExpandRatio(1);
		userGrid.addComponentColumn(this::buildButtons).setExpandRatio(2);

		ok.addClickListener(e -> {
			pagination.firePagedChangedEvent();
		});
		cancel.addClickListener(e -> {
			presenter.getBinder().readBean(null);
			active.setValue(true);
			pagination.firePagedChangedEvent();
		});
		add.addClickListener(e -> newEmployee());
	}
	
	public ComboBoxMultiselect<Role> getRoles(){
		return roles;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public Grid<Employee> getGrid(){
		return userGrid;
	}

	private Label buildLockedLabel(Boolean source) {
		Label image = new Label();
		image.setContentMode(ContentMode.HTML);
		image.setValue(VaadinIcons.CIRCLE.getHtml());
		image.addStyleName(source ? "locked" : "active");
		image.setDescription(source ? "Заблокирована" : "Активная");
		return image;
	}

	private HorizontalLayout buildButtons(Employee employee) {
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		Button view = createButton(VaadinIcons.EYE, "Просмотр", employee.getLogin());
		Button edit = createButton(VaadinIcons.PENCIL, "Редактирование", employee.getLogin());
		Button lock = createButton(VaadinIcons.BAN, "Заблокировать", employee.getLogin());
		Button unlock = createButton(VaadinIcons.UNLOCK, "Разблокировать", employee.getLogin());

		if (employee.isLocked()) {
			horizontalLayout.addComponent(view);
			horizontalLayout.addComponent(unlock);
			if (employee.getOrganization().isLocked()) {
				unlock.setEnabled(false);
				unlock.setDescription("Нельзя разблокировать пользователя! Он относится к заблокированной организации");
			}
		}else {
			horizontalLayout.addComponent(view);
			horizontalLayout.addComponent(edit);
			if (!employee.getLogin().equals(SecurityUtils.getUsername())) {
				horizontalLayout.addComponent(lock);				
			}
		}

		view.addClickListener(e -> openEmployeeCard((String) e.getButton().getData(), Mode.VIEW));
		edit.addClickListener(e -> openEmployeeCard((String) e.getButton().getData(), Mode.EDIT));
        lock.addClickListener(e -> presenter.lockPressed(e));
        unlock.addClickListener(e -> presenter.unlockPressed(e));
		return horizontalLayout;
	}

	private static Button createButton(VaadinIcons icon, String description, String login) {
		Button button = new Button(icon);
		button.addStyleName(ValoTheme.BUTTON_TINY);
		button.setDescription(description);
		button.setData(login);
		return button;
	}

	public void newEmployee() {
		navigationManager.navigateTo(UserCardView.class);
	}

	public void openEmployeeCard(String id, Mode mode) {
		navigationManager.navigateTo(UserCardView.class, id, mode);
	}
}
