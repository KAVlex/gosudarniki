package ru.gbu.sr.ui.views;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.BindingValidationStatus;
import com.vaadin.data.HasValue;
import com.vaadin.navigator.ViewBeforeLeaveEvent;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Component.Focusable;
import com.vaadin.ui.Notification.Type;

import ru.gbu.sr.ui.components.ConfirmPopup;
import ru.gbu.sr.ui.navigation.NavigationManager;
import ru.gbu.sr.ui.views.Mode;

@SpringComponent
@ViewScope
public abstract class AbstractCardPresenter implements Serializable{

	private static final long serialVersionUID = 1L;
	protected final NavigationManager navigationManager;
	protected final ConfirmPopup confirmPopup;
	protected AbstractCardView view;
	protected static final String ALIGN_CENTER = "v-align-center";
	private boolean hasChanges;
	
	@Autowired
	public AbstractCardPresenter(NavigationManager navigationManager, ConfirmPopup confirmPopup) {
		this.navigationManager = navigationManager;
		this.confirmPopup = confirmPopup;
	}
	
	protected void init(AbstractCardView view) {
		this.view = view;
	}
	
	public void enter(ViewChangeEvent event) {
		Long orgId = getId(event.getParameters());
		Mode mode = getMode();
		view.setMode(mode);
		if (mode == null) {
			Notification.show("Неверный режим, обратитесь к администратору", Type.ERROR_MESSAGE);
		}else {
			enterView(orgId);
		}
	}
	
	protected abstract void enterView(Long id);
	
	public abstract void cancelPressed();
	
	public abstract void savePressed();
	
	public abstract void saveExitPressed();
	
	public abstract void lockPressed();
	
	public abstract void unlockPressed();
	
	protected boolean readyToSave() {
		if (containsUnsavedChanges()) {
			Optional<HasValue<?>> firstErrorField = validate().findFirst();
			if (firstErrorField.isPresent()) {
				((Focusable) firstErrorField.get()).focus();
				return false;
			}
			return true;
		}else {
			Notification.show("Нет изменений");
			return false;
		}
	}
	
	public Stream<HasValue<?>> validate() {
		Stream<HasValue<?>> errorFields = view.getBinder().validate().getFieldValidationErrors().stream()
				.map(BindingValidationStatus::getField);
		return errorFields;
	}
	
	public Long getId(String value) {
		String orgId = navigationManager.getParameter(value);
		return "".equals(orgId) ? null : Long.valueOf(orgId);
	}
	
	public Mode getMode() {
		return navigationManager.getMode();
	}
	
	public boolean containsUnsavedChanges() {
		return hasChanges;
	}
	
	public void setNoChanges() {
		hasChanges = false;
	}
	
	public void hasChanges() {
		hasChanges = true;
	}
	
	public void beforeLeave(ViewBeforeLeaveEvent event) {
		event.navigate();
	}
	
}
