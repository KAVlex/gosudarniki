package ru.gbu.sr.ui.views.users;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.Label;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.DateField;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Button;

@DesignRoot
public class UserCardViewDesign extends VerticalLayout {
	private static final long serialVersionUID = 1L;
	protected Label modeLabel;
	protected ComboBox<ru.gbu.sr.backend.data.entity.Organization> organization;
	protected TextField lastName;
	protected TextField firstName;
	protected TextField patronymic;
	protected TextField login;
	protected Button generateLogin;
	protected TextField passwordHash;
	protected Button generatePassword;
	protected TextField cert;
	protected Button clearCert;
	protected TextField snils;
	protected TextField inn;
	protected DateField birthDate;
	protected TextField position;
	protected TextField contactPhone;
	protected TextField email;
	protected CheckBox notifications;
	protected CheckBox operator;
	protected CheckBox administrator;
	protected CheckBox declarant;
	protected CheckBox executor;
	protected CheckBox manager;
	protected CheckBox supervisor;
	protected CheckBox supersupervisor;
	protected TextField updater;
	protected TextField dateUpdated;
	protected Button cancel;
	protected Button edit;
	protected Button save;
	protected Button saveExit;
	protected Button unlock;
	protected Button lock;

	public UserCardViewDesign() {
		Design.read(this);
	}

}
