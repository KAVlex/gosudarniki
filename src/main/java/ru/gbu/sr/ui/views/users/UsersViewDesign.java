package ru.gbu.sr.ui.views.users;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.declarative.Design;

import ru.gbu.sr.backend.data.Role;
import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.ui.components.Pagination;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.DateTimeField;
import org.vaadin.addons.ComboBoxMultiselect;

@DesignRoot
public class UsersViewDesign extends VerticalLayout {
	private static final long serialVersionUID = 1L;
	protected Button add;
    protected TextField organization;
    protected TextField lastName;
    protected TextField snils;
    protected CheckBox active;
    protected CheckBox online;
    protected DateTimeField dateStart;
    protected DateTimeField dateEnd;
    protected ComboBoxMultiselect<Role> roles;
    protected Button ok;
    protected Button cancel;
    protected Pagination pagination;
    protected Grid<Employee> userGrid;

    public UsersViewDesign() {
		Design.read(this);
	}

}
