package ru.gbu.sr.ui.views.services;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.vaadin.addon.pagination.PaginationChangeListener;
import com.vaadin.addon.pagination.PaginationResource;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.ValidationException;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import ru.gbu.sr.backend.data.ServiceFind;
import ru.gbu.sr.ui.components.Pagination;
import ru.gbu.sr.ui.navigation.NavigationManager;
import ru.gbu.sr.ui.views.Mode;

@SpringComponent
@ViewScope
public class ServicesPresenter implements Serializable {

	private static final long serialVersionUID = 1L;
	private ServicesView view;
	private final NavigationManager navigationManager;
	private final ServicesDataProvider dataProvider;
	private BeanValidationBinder<ServiceFind> binder;

	@Autowired
	public ServicesPresenter(NavigationManager navigationManager, ServicesDataProvider dataProvider) {
		this.navigationManager = navigationManager;
		this.dataProvider = dataProvider;
	}

	void init(ServicesView view) {
		this.view = view;
		view.getGrid().setDataProvider(dataProvider);
		binder = new BeanValidationBinder<>(ServiceFind.class);
		binder.bindInstanceFields(view);
		int services = findAll(new ServiceFind(), 0, Pagination.DEFAULT_LIMIT);
		view.getPagination().init(services);
		view.getPagination().addPageChangeListener(new PaginationChangeListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void changed(PaginationResource event) {
				find(event.pageIndex(), event.limit());
			}
		});
	}

	public void find(int page, int limit) {
		ServiceFind serviceFind = new ServiceFind();
		try {
			binder.writeBean(serviceFind);
		} catch (ValidationException e) {
			Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
		int orgs = findAll(serviceFind, page, limit);
		view.getPagination().setTotalCount(orgs);
	}

	private int findAll(ServiceFind serviceFind, int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
		return dataProvider.filter(serviceFind, pageable);
	}
	
	public void newService() {
		navigationManager.navigateTo(ServiceCardView.class);
	}

	public void openServiceCard(Long id, Mode mode) {
		navigationManager.navigateTo(ServiceCardView.class, id, mode);
	}

	public BeanValidationBinder<ServiceFind> getBinder() {
		return binder;
	}
}
