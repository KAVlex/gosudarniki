package ru.gbu.sr.ui.views.organizations;

import java.util.Collection;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.gridutil.cell.GridCellFilter;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder.BindingBuilder;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewBeforeLeaveEvent;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.renderers.ComponentRenderer;
import com.vaadin.ui.themes.ValoTheme;

import ru.gbu.sr.app.security.SecurityUtils;
import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.backend.data.entity.Organization;
import ru.gbu.sr.ui.components.ConfirmPopup;
import ru.gbu.sr.ui.utils.DateToStringConvertor;
import ru.gbu.sr.ui.utils.LongToStringConvertor;
import ru.gbu.sr.ui.views.LockedValueProvider;
import ru.gbu.sr.ui.views.Mode;
import ru.gbu.sr.ui.views.users.UserCardView;

@SpringView
@Secured({"Operator"})
public class OrganizationCardView extends OrganizationCardViewDesign implements View{
	
	private static final long serialVersionUID = 1L;
	private Mode mode;
	private final OrganizationCardPresenter presenter;
	private BeanValidationBinder<Organization> binder;
	private final BeanFactory beanFactory;
	private boolean hasChanges;
	private static final String ALIGN_CENTER = "v-align-center";
	
	@Autowired
	public OrganizationCardView(OrganizationCardPresenter presenter, BeanFactory beanFactory) {
		this.presenter = presenter;
		this.beanFactory = beanFactory;
	}
	
	@PostConstruct
	public void init() {
		presenter.init(this);
		binder = new BeanValidationBinder<>(Organization.class);
		binder.forField(id).withConverter(new LongToStringConvertor()).bind("id");
		binder.forField(dateUpdated).withConverter(new DateToStringConvertor()).bind("dateUpdated");
		binder.forField(isHead).bind(this::isHead, this::setParent);
		binder.forField(updater).bind(organization -> { 
			return (organization.getUpdater() != null) ? organization.getUpdater().getLogin() : "";
		}, null);
		parent.setItemCaptionGenerator(Organization::getName);
		addValidators();
		binder.bindInstanceFields(this);
		binder.addValueChangeListener(e -> hasChanges = true);

		addOrgColumns();
		addEmployeeColumns();
		
		cancel.addClickListener(e -> presenter.cancelPressed());
		save.addClickListener(e -> presenter.savePressed());
		saveExit.addClickListener(e -> presenter.saveExitPressed());
		edit.addClickListener(e -> presenter.editPressed());
		lock.addClickListener(e -> presenter.lockPressed());
		unlock.addClickListener(e -> presenter.unlockPressed());
		addUser.addClickListener(e -> presenter.addUserPressed());
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		Long orgId = presenter.getId(event.getParameters());
		setMode(presenter.getMode());
		if (mode == null) {
			Notification.show("Неверный режим, обратитесь к администратору", Type.ERROR_MESSAGE);
		}else {
			presenter.enterView(orgId);
		}
	}
	
	private void addValidators(){
		binder.forMemberField(parent).withValidator((value, c) -> {
			return isHead.getValue() || parent.getValue() != null ? ValidationResult.ok() : 
				ValidationResult.error("Необходимо выбрать 'Подчинение'");
		});
		addValidator(name, "Значение поля 'Наименование' должно содержать не менее 2 символов", "^\\S+[\\S\\s]*\\S+$");
		addValidator(registryNumber, "Значение поля 'Реестровый номер' должно содержать 19 цифр", "(^$|^[0-9]{19}$)");
		addValidator(kbk, "Значение поля 'КБК' должно содержать 20 цифр", "(^$|^[0-9]{20}$)");
		addValidator(ogrn, "Значение поля 'ОГРН' должно содержать 13 цифр", "^[0-9]{13}$");
		addValidator(inn, "Значение поля 'ИНН' должно содержать 10 цифр", "^[0-9]{10}$");
		addValidator(oktmo, "Значение поля 'ОКТМО' должно содержать 8 или 11 цифр", "^([0-9]{8})|([0-9]{11})$");
		addValidator(kpp, "Значение поля 'КПП' должно содержать 9 цифр", "(^$|^[0-9]{9}$)");
		addValidator(rs, "Значение поля '№ р/с' должно содержать 20 цифр", "(^$|^[0-9]{20}$)");
		addValidator(ks, "Значение поля '№ к/с' должно содержать 20 цифр", "(^$|^[0-9]{20}$)");
		addValidator(bik, "Значение поля 'БИК' должно содержать 9 цифр", "(^$|^[0-9]{9}$)");
		addValidator(tel, "Значение поля 'Телефон' должно содержать не менее 2 символов", "^\\S+[\\S\\s]*\\S+$");
		binder.forMemberField(email).withValidator(new EmailValidator("Поле 'e-mail' заполнено неверно"));
	}
	
	private BindingBuilder<Organization, String> addValidator(AbstractTextField field, String text, String regexp) {
		return binder.forMemberField(field).withValidator(new RegexpValidator(text, regexp));
	}
	
	private void addOrgColumns() {
		Column<Organization, Boolean> lockedColumn = (Column<Organization, Boolean>) organizations.addColumn(Organization::isLocked);
		lockedColumn.setStyleGenerator(item -> ALIGN_CENTER);
		lockedColumn.setRenderer(new LockedValueProvider(), new ComponentRenderer());
		
		lockedColumn.setCaption("").setResizable(true).setHidable(true);
		organizations.addColumn(Organization::getName).setId("name").setCaption("Наименование").setResizable(true);
		organizations.addColumn(Organization::getId).setCaption("Код").setResizable(true).setHidable(true);
		organizations.addColumn(Organization::getRegistryNumber).setCaption("Реестровый номер").setResizable(true).setHidable(true);
		organizations.addColumn(Organization::getDate).setCaption("Дата создания").setResizable(true).setHidable(true);
		organizations.addColumn(Organization::getOktmo).setCaption("ОКТМО").setResizable(true).setHidable(true);
		organizations.addColumn(Organization::getInn).setCaption("ИНН").setResizable(true).setHidable(true);
		organizations.addComponentColumn(this::buildViewOrgButton);
	}
	
	private void addEmployeeColumns() {
		Column<Employee, Boolean> lockedCol = (Column<Employee, Boolean>) employees.addColumn(Employee::isLocked);
		lockedCol.setStyleGenerator(item -> ALIGN_CENTER);
		lockedCol.setRenderer(new LockedValueProvider(), new ComponentRenderer());
		lockedCol.setCaption("").setResizable(true).setHidable(true);
		employees.addColumn(Employee::getLogin).setId("login").setCaption("Логин").setResizable(true);
		employees.addColumn(Employee::getFio).setId("fio").setCaption("ФИО").setResizable(true);
		employees.addColumn(Employee::getBirthDate).setCaption("Дата рождения").setResizable(true).setHidable(true);
		employees.addColumn(Employee::getSnils).setCaption("СНИЛС").setResizable(true).setHidable(true);
		employees.addColumn(Employee::getDate).setCaption("Дата создания").setResizable(true).setHidable(true);
		employees.addColumn(this::getRoleNames).setCaption("Роль").setResizable(true).setHidable(true);
		employees.addComponentColumn(this::buildViewEmployeeButton);
	}
	
	private String getRoleNames(Employee employee) {
		Set<String> roleNames = employee.getRoleNames();
        return roleNames.toString().replace("[", "").replaceAll("]", "");
	}
	
	private HorizontalLayout buildViewOrgButton(Organization organization) {
    	return createViewButton(organization.getId(), OrganizationCardView.class);
    }
	
	private HorizontalLayout buildViewEmployeeButton(Employee employee) {
        return createViewButton(employee.getLogin(), UserCardView.class);
    }
	
	private HorizontalLayout createViewButton(Object data, Class<? extends View> cl) {
    	HorizontalLayout horizontalLayout = new HorizontalLayout();
    	Button view = new Button(VaadinIcons.EYE);
    	view.addStyleName(ValoTheme.BUTTON_TINY);
    	view.setDescription("Просмотр");
    	view.setData(data);
    	view.addClickListener(e -> presenter.navigateTo(cl, e.getButton().getData(), Mode.VIEW));
    	horizontalLayout.addComponent(view);
        return horizontalLayout;
	}
	
	private boolean isHead(Organization organization) {
		return organization.getParent() == null && !organization.isNew();
	}
	
	private void setParent(Organization organization, Boolean head) {
		parent.setRequiredIndicatorVisible(!head);
		parent.setVisible(!head);
		if (head) {
			parent.setValue(null);			
		}
	}
	
	public Organization getParentOrg() {
		return parent.getValue();
	}
	
	public void setOrganization(Organization organization) {
		binder.setBean(organization);
		setParent(organization, isHead(organization));
		employees.setItems(organization.getEmployees());
		initEmployeesFilter();
		organizations.setItems(organization.getOrganizations());
		initOrgsFilter();
		lock.setVisible(organization.getId() != null && !organization.isLocked()
				&& !organization.getId().equals(SecurityUtils.getCurrentUser().getOrganization().getId()));
		unlock.setVisible(organization.isLocked());
		if (organization.isLocked()) {
			edit.setVisible(false);
		}
		hasChanges = false;
	}
	
	private void initEmployeesFilter() {
		GridCellFilter<Employee> filter = new GridCellFilter<Employee>(employees, Employee.class);
		filter.setTextFilter("login", true, false);
		filter.setTextFilter("fio", true, false);
	}
	
	private void initOrgsFilter() {
		GridCellFilter<Organization> filter = new GridCellFilter<Organization>(organizations, Organization.class);
		filter.setTextFilter("name", true, false);
	}
	
	protected Organization getOrganization() {
		return binder.getBean();
	}
	
	protected BeanValidationBinder<Organization> getBinder(){
		return binder;
	}
	
	public void showNotFound() {
		removeAllComponents();
		addComponent(new Label("Организация не найдена"));
	}
	
	public void setMode(Mode mode) {
		this.mode = mode;
		edit.setVisible(mode.equals(Mode.VIEW));
		save.setVisible(!mode.equals(Mode.VIEW));
		saveExit.setVisible(!mode.equals(Mode.VIEW));
		lock.setVisible(!mode.equals(Mode.ADD));
		accordionOrg.setVisible(!mode.equals(Mode.ADD));
		binder.setReadOnly(mode.equals(Mode.VIEW));
		dateUpdated.setReadOnly(true);
		id.setReadOnly(true);
		switch (mode) {
			case ADD:
				modeLabel.setValue("Создание организации");
				break;
			case EDIT:
				modeLabel.setValue("Редактирование организации");
				break;
			default:
				modeLabel.setValue("Просмотр организации");
				break;
		}
	}
	
	public void setParentItems(Collection<Organization> items) {
		parent.setItems(items);
	}
	
	public boolean containsUnsavedChanges() {
		return hasChanges;
	}
	
	public void setNoChanges() {
		hasChanges = false;
	}
	
	public ConfirmPopup getConfirmPopup() {
		return beanFactory.getBean(ConfirmPopup.class);
	}
	
	@Override
	public void beforeLeave(ViewBeforeLeaveEvent event) {
		if (!containsUnsavedChanges()) {
			event.navigate();
		} else {
			ConfirmPopup confirmPopup = getConfirmPopup();
			confirmPopup.showLeaveViewConfirmDialog(this, event::navigate);
		}
	}
}
