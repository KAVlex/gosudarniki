package ru.gbu.sr.ui.views.organizations;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.renderers.ComponentRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import com.vaadin.ui.themes.ValoTheme;

import ru.gbu.sr.app.security.SecurityUtils;
import ru.gbu.sr.backend.data.entity.Organization;
import ru.gbu.sr.ui.Cacheable;
import ru.gbu.sr.ui.components.Pagination;
import ru.gbu.sr.ui.views.LockedValueProvider;
import ru.gbu.sr.ui.views.Mode;

@SpringView
@Secured({"Operator"})
public class OrganizationsView extends OrganizationsViewDesign implements View, Cacheable{
	
	private static final long serialVersionUID = 1L;
	
	private final OrganizationsPresenter presenter;

	@Autowired
	public OrganizationsView(OrganizationsPresenter presenter) {
		this.presenter = presenter;
	}
	
	@PostConstruct
	public void init() {
		presenter.init(this);
		
		startDate.setLocale(new Locale("ru", "RU"));
		endDate.setLocale(new Locale("ru", "RU"));
		
		organizationsGrid.setColumns("locked", "name", "id", "registryNumber", "date", "oktmo", "inn", "parent");
		
		@SuppressWarnings("unchecked")
		Column<Organization, Boolean> lockedColumn = (Column<Organization, Boolean>) organizationsGrid.getColumn("locked");
		lockedColumn.setStyleGenerator(item -> "v-align-center");
		lockedColumn.setRenderer(new LockedValueProvider(), new ComponentRenderer());
		lockedColumn.setCaption("").setResizable(true).setHidable(true).setExpandRatio(0);
		organizationsGrid.getColumn("name").setCaption("Наименование").setResizable(true).setExpandRatio(1).
			setDescriptionGenerator(Organization::getName);
		organizationsGrid.getColumn("id").setCaption("Код").setResizable(true).setHidable(true).setExpandRatio(0);
		organizationsGrid.getColumn("registryNumber").setCaption("Реестровый номер").setResizable(true).setHidable(true).setExpandRatio(1);
		organizationsGrid.getColumn("date").setCaption("Дата создания").setResizable(true).setHidable(true).setExpandRatio(1);
		organizationsGrid.getColumn("oktmo").setCaption("ОКТМО").setResizable(true).setHidable(true).setExpandRatio(1);
		organizationsGrid.getColumn("inn").setCaption("ИНН").setResizable(true).setHidable(true).setExpandRatio(1);
		organizationsGrid.getColumn("parent").setCaption("Подчинение").setResizable(true).setHidable(true)
				.setRenderer(org -> org == null ? "" : ((Organization) org).getName(), new TextRenderer()).setExpandRatio(1);
		organizationsGrid.addComponentColumn(this::buildButtons).setExpandRatio(2);
		
		ok.addClickListener(e -> {
			pagination.firePagedChangedEvent();
		});
		cancel.addClickListener(e -> {
			presenter.getBinder().readBean(null);
			active.setValue(true);
			pagination.firePagedChangedEvent();
		});
		add.addClickListener(e -> presenter.newOrganization());
	}
	
	public Pagination getPagination() {
		return pagination;
	}
	
	public Grid<Organization> getGrid(){
		return organizationsGrid;
	}
	
	/**
    	<vaadin-button style-name="tiny" icon="fonticon://Vaadin-Icons/e617"></vaadin-button>
		<vaadin-button style-name="tiny" icon="fonticon://Vaadin-Icons/e7df"></vaadin-button>
		<vaadin-button style-name="tiny" icon="fonticon://Vaadin-Icons/e7fa"></vaadin-button>
		<vaadin-button style-name="tiny" icon="fonticon://Vaadin-Icons/e6f7"></vaadin-button>
	 */
	private HorizontalLayout buildButtons(Organization organization) {
    	HorizontalLayout horizontalLayout = new HorizontalLayout();
    	Button users = createButton(VaadinIcons.USER, "Создать пользователя", organization.getId());
    	Button view = createButton(VaadinIcons.EYE, "Просмотр", organization.getId());
    	Button edit = createButton(VaadinIcons.PENCIL, "Редактирование", organization.getId());
    	Button lock = createButton(VaadinIcons.BAN, "Заблокровать", organization.getId());
    	Button unlock = createButton(VaadinIcons.UNLOCK, "Разблокировать", organization.getId());
    	
    	if (organization.isLocked()) {
    		horizontalLayout.addComponent(view);
    		horizontalLayout.addComponent(unlock);	
    	}else {
    		horizontalLayout.addComponent(users);
        	horizontalLayout.addComponent(view);
        	horizontalLayout.addComponent(edit);
        	if (!organization.getId().equals(SecurityUtils.getCurrentUser().getOrganization().getId())) {
        		horizontalLayout.addComponent(lock);	        		
        	}
    	}
       
    	users.addClickListener(e -> presenter.addUserPressed((Long) e.getButton().getData()));
    	view.addClickListener(e -> presenter.openOrfanizationCard((Long) e.getButton().getData(), Mode.VIEW));
    	edit.addClickListener(e -> presenter.openOrfanizationCard((Long) e.getButton().getData(), Mode.EDIT));
    	lock.addClickListener(e -> presenter.lockPressed(e));
    	unlock.addClickListener(e -> presenter.unlockPressed(e));
        return horizontalLayout;
    }
	
	private static Button createButton(VaadinIcons icon, String description, Long id) {
    	Button button = new Button(icon);
    	button.addStyleName(ValoTheme.BUTTON_TINY);
    	button.setDescription(description);
    	button.setData(id);
    	return button;
	}
	
}
