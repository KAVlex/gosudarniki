package ru.gbu.sr.ui.views.projects;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder.BindingBuilder;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.navigator.ViewBeforeLeaveEvent;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Label;

import ru.gbu.sr.backend.data.entity.DefinitionStatus;
import ru.gbu.sr.backend.data.entity.Project;
import ru.gbu.sr.backend.data.entity.ProjectEstimate;
import ru.gbu.sr.ui.utils.DateToStringConvertor;
import ru.gbu.sr.ui.utils.LongToStringConvertor;
import ru.gbu.sr.ui.views.AbstractCardView;
import ru.gbu.sr.ui.views.Mode;

@SpringView
@Secured({"Operator", "Executor"})
public class ProjectCardView extends ProjectCardViewDesign implements AbstractCardView{

	private static final long serialVersionUID = 1L;
	private final ProjectCardPresenter presenter;
	private BeanValidationBinder<Project> binder;
	public ListDataProvider<ProjectEstimate> estimatesDataProvider;
	public List<ProjectEstimate> estimates;

	
	@Autowired
	public ProjectCardView(ProjectCardPresenter presenter) {
		this.presenter = presenter;
	}
	
	@PostConstruct
	public void init() {
		presenter.init(this);
		binder = new BeanValidationBinder<>(Project.class);
		binder.forField(updater).bind(service -> { 
			return (service.getUpdater() != null) ? service.getUpdater().getLogin() : "";
		}, null);
		binder.forField(dateUpdated).withConverter(new DateToStringConvertor()).bind("dateUpdated");
		addValidators();
		binder.forField(rating).withConverter(new LongToStringConvertor()).bind("rating");
		binder.forField(cost).withConverter(new LongToStringConvertor()).bind("cost");
		binder.bindInstanceFields(this);
		binder.addValueChangeListener(e -> presenter.hasChanges());
		
		estimates = new ArrayList<ProjectEstimate>();
		estimatesDataProvider = new ListDataProvider<>(estimates);
		estimateGrid.setDataProvider(estimatesDataProvider);
		
		estimateGrid.addColumn(ProjectEstimate::getName).setCaption("Наименование");
		estimateGrid.addColumn(ProjectEstimate::getCount).setCaption("Количество");
		estimateGrid.addColumn(ProjectEstimate::getPrice).setCaption("Стоимость за единицу");
		estimateGrid.addColumn(ProjectEstimate::getCost).setCaption("Общая стоимость");
		
		cancel.addClickListener(e -> presenter.cancelPressed());
		save.addClickListener(e -> presenter.savePressed());
		saveExit.addClickListener(e -> presenter.saveExitPressed());
		edit.addClickListener(e -> presenter.editPressed());
		reject.addClickListener(e -> presenter.lockPressed());
		accept.addClickListener(e -> presenter.unlockPressed());
		
		saveEstimate.addClickListener(e -> {
			ProjectEstimate projectEstimate = new ProjectEstimate();
			projectEstimate.setName(estimateName.getValue());
			projectEstimate.setCount(Long.valueOf(estimateCount.getValue()));
			projectEstimate.setPrice(Long.valueOf(estimatePrice.getValue()));
			projectEstimate.setProject(getProject());
			estimates.add(projectEstimate);
			estimatesDataProvider.refreshAll();
			estimateName.clear();
			estimatePrice.clear();
			estimateCount.clear();
			presenter.hasChanges();
			Long newCostTotal = Long.valueOf(cost.getValue()) + projectEstimate.getCost();
			cost.setValue(newCostTotal.toString());
		});
		
		defstatus.setItems(DefinitionStatus.values());
		defstatus.setItemCaptionGenerator(DefinitionStatus::getLabelName);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		presenter.enter(event);
	}
	
	private void addValidators(){
		addValidator(name, "Значение поля 'Наименование' должно содержать не менее 2 символов", "^\\S+[\\S\\s]*\\S+$");
		binder.forMemberField(cost)
			.withConverter(new LongToStringConvertor()).bind("cost");
	}
	
	public void showNotFound() {
		removeAllComponents();
		addComponent(new Label("Процедура не найдена"));
	}
	
	public void setService(Project service) {
		binder.setBean(service);
		presenter.setNoChanges();
	}
	
	public void setMode(Mode mode) {
		binder.setReadOnly(!mode.equals(Mode.ADD));
		defstatus.setReadOnly(mode.equals(Mode.VIEW));
		estimateCount.setReadOnly(mode.equals(Mode.VIEW));
		estimateName.setReadOnly(mode.equals(Mode.VIEW));
		estimatePrice.setReadOnly(mode.equals(Mode.VIEW));
		saveEstimate.setVisible(mode.equals(Mode.EDIT) || mode.equals(Mode.ADD));
		comment.setReadOnly(mode.equals(Mode.VIEW));
		edit.setVisible(mode.equals(Mode.VIEW));
		saveExit.setVisible(!mode.equals(Mode.VIEW));
		save.setVisible(!mode.equals(Mode.VIEW));
		switch (mode) {
			case ADD:
				modeLabel.setValue("Создание проекта");
				break;
			case EDIT:
				modeLabel.setValue("Редактирование проекта");
				break;
			default:
				modeLabel.setValue("Просмотр проекта");
				break;
		}
	}
	
	public BeanValidationBinder<Project> getBinder(){
		return binder;
	}	
	
	@Override
	public void beforeLeave(ViewBeforeLeaveEvent event) {
		presenter.beforeLeave(event);
	}
	
	public Project getProject() {
		return binder.getBean();
	}
	
	public BindingBuilder<Project, String> addValidator(AbstractTextField field, String text, String regexp) {
		return binder.forMemberField(field).withValidator(new RegexpValidator(text, regexp));
	}
	
}
