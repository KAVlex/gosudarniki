package ru.gbu.sr.ui.views;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.navigator.View;

import ru.gbu.sr.ui.views.Mode;

public interface AbstractCardView extends View{

	public void showNotFound();
	
	public void setMode(Mode mode);
	
	public BeanValidationBinder<?> getBinder();
	
}
