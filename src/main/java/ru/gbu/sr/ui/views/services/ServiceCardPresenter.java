package ru.gbu.sr.ui.views.services;

import java.io.Serializable;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.OptimisticLockingFailureException;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import ru.gbu.sr.backend.data.entity.Service;
import ru.gbu.sr.backend.service.ServiceService;
import ru.gbu.sr.backend.service.UserFriendlyDataException;
import ru.gbu.sr.ui.components.ConfirmPopup;
import ru.gbu.sr.ui.navigation.NavigationManager;
import ru.gbu.sr.ui.views.AbstractCardPresenter;
import ru.gbu.sr.ui.views.Mode;

@SpringComponent
@ViewScope
public class ServiceCardPresenter extends AbstractCardPresenter implements Serializable{

	private static final long serialVersionUID = 1L;
	private final ServiceService serviceService;
	private ServiceCardView view;
	@Autowired
	ApplicationContext applicationContext; 
	
	@Autowired
	public ServiceCardPresenter(NavigationManager navigationManager, ConfirmPopup confirmPopup, ServiceService serviceService) {
		super(navigationManager, confirmPopup);
		this.serviceService = serviceService;
	}
	
	public void init(ServiceCardView view) {
		super.init(view);
		this.view = view;
	}
	
	@Override
	public void enterView(Long id) {
		Service service;
		if (id == null) {
			service = new Service();
			view.setMode(Mode.ADD);
		} else {
			service = serviceService.findById(id);
			if (service == null) {
				view.showNotFound();
				return;
			}
		}
		refreshView(service);
	}
	
	private void refreshView(Service service) {
		view.setService(service);
	}
	
	@Override
	public void cancelPressed() {
		navigationManager.navigateTo(ServicesView.class);
	}
	
	@Override
	public void savePressed() {
		Service service = save();
		if (service != null) {
			navigationManager.navigateTo(ServiceCardView.class, service.getId().toString(), Mode.EDIT);
		}
	}
	
	@Override
	public void saveExitPressed() {
		Service service = save();
		if (service != null) {
			navigationManager.navigateTo(ServicesView.class);
		}
	}
	
	@Override
	public void lockPressed() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void unlockPressed() {
		throw new UnsupportedOperationException();
	}
	
	public void editPressed() {
		Service service = view.getService();
		navigationManager.navigateTo(ServiceCardView.class, service.getId().toString(), Mode.EDIT);
	}
	
	private Service save() {
		if (readyToSave()) {
			try {
				Service service = view.getService();
				service = serviceService.save(service);
				if (service != null) {
					setNoChanges();
					Notification.show("Сохранено " + service.getId());
					return service;
				}
			} catch (UserFriendlyDataException e) {
				Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
			} catch (ValidationException e) {
				Notification.show("Пожалуйста проверьте заполнение полей: " + e.getMessage(), Type.ERROR_MESSAGE);
			} catch (OptimisticLockingFailureException e) {
				Notification.show("Возможно, кто-то еще мог обновить данные. Обновите страницу и повторите попытку.",
						Type.ERROR_MESSAGE);
			} catch (Exception e) {
				Notification.show(
						"При сохранении произошла непредвиденная ошибка. Обновите страницу и повторите попытку.",
						Type.ERROR_MESSAGE);
			}
		}
		return null;
	}
	
}
