package ru.gbu.sr.ui.views.organizations;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;

import com.vaadin.data.BindingValidationStatus;
import com.vaadin.data.HasValue;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Component.Focusable;
import com.vaadin.ui.Notification.Type;

import ru.gbu.sr.backend.data.entity.Organization;
import ru.gbu.sr.backend.service.OrganizationService;
import ru.gbu.sr.ui.components.ConfirmPopup;
import ru.gbu.sr.ui.navigation.NavigationManager;
import ru.gbu.sr.ui.views.Mode;
import ru.gbu.sr.ui.views.users.UserCardView;


@SpringComponent
@ViewScope
public class OrganizationCardPresenter implements Serializable{

	private static final long serialVersionUID = 1L;

	private final OrganizationService organizationService;
	private OrganizationCardView view;
	private final NavigationManager navigationManager;
	
	@Autowired
	public OrganizationCardPresenter(NavigationManager navigationManager, OrganizationService organizationService) {
		this.navigationManager = navigationManager;
		this.organizationService = organizationService;
	}
	
	public void init(OrganizationCardView view) {
		this.view = view;
	}
	
	public void enterView(Long id) {
		Organization org;
		if (id == null) {
			org = new Organization();
			view.setParentItems(organizationService.findAllActive());
			view.setMode(Mode.ADD);
		} else {
			org = organizationService.findById(id);
			if (org == null) {
				view.showNotFound();
				return;
			}
			view.setParentItems(organizationService.findAllActiveExcludeId(org.getId()));
		}
		
		refreshView(org);
		
		if (org.getParent() != null && org.getParent().isLocked()) {
			Notification.show("Головная организация заблокирована, выберите другую или измените уровень подчинения!");
		}
	}
	
	private void refreshView(Organization organization) {
		view.setOrganization(organization);
	}
	
	public void cancelPressed() {
		navigationManager.navigateTo(OrganizationsView.class);
	}
	
	public void savePressed() {
		Organization organization = save();
		if (organization != null) {
			navigationManager.navigateTo(OrganizationCardView.class, organization.getId().toString(), Mode.EDIT);
		}
	}
	
	public void saveExitPressed() {
		Organization organization = save();
		if (organization != null) {
			navigationManager.navigateTo(OrganizationsView.class);
		}
	}
	
	public Organization save() {
		if (view.containsUnsavedChanges()) {
			Optional<HasValue<?>> firstErrorField = validate().findFirst();
			if (firstErrorField.isPresent()) {
				((Focusable) firstErrorField.get()).focus();
				return null;
			}
			if (view.getParentOrg() != null && view.getParentOrg().isLocked()) {
				Notification.show(
						"Головная организация заблокирована, выберите другую или измените уровень подчинения!",
						Type.ERROR_MESSAGE);
				return null;
			}
			Organization organization = saveOrganization();
			if (organization != null) {
				view.setNoChanges();
				Notification.show("Сохранено " + organization.getId());
				return organization;
			}
		}else {
			Notification.show("Нет изменений");
		}
		return null;
	}
	
	public void editPressed() {
		Organization organization = view.getOrganization();
		navigationManager.navigateTo(OrganizationCardView.class, organization.getId().toString(), Mode.EDIT);
	}
	
	public void lockPressed() {
		Organization organization = view.getOrganization();
		ConfirmPopup confirmPopup = view.getConfirmPopup();
		confirmPopup.showConfirmDialog(view, "Заблокировать организацию и имеющихся в ней пользователей?", () -> {
			if (organizationService.lock(organization)) {
				Notification.show("Организация заблокирована");
				navigationManager.navigateTo(OrganizationsView.class);					
			}else {
				Notification.show("Не удалось заблокировать организацию, обратитесь к администратору", Type.ERROR_MESSAGE);
			}
		});
	}
	
	public void unlockPressed() {
		Organization organization = view.getOrganization();
		ConfirmPopup confirmPopup = view.getConfirmPopup();
		confirmPopup.showConfirmDialog(view, "Восстановить организацию? Пользователей организации необходимо добавить вручную!", () -> {
			if (organizationService.unlock(organization)) {
				Notification.show("Организация восстановлена");
				navigationManager.navigateTo(OrganizationsView.class);					
			}else {
				Notification.show("Не удалось восстановить организацию, обратитесь к администратору", Type.ERROR_MESSAGE);
			}
		});
	}
	
	public void addUserPressed() {
		Organization organization = view.getOrganization();
		navigationManager.navigateTo(UserCardView.class, "&org=" + organization.getId(), Mode.ADD);
	}
	
	public void navigateTo(Class<? extends View> targetView, Object parameter, Mode mode) {
		navigationManager.navigateTo(targetView, parameter, mode);
	}
	
	public Stream<HasValue<?>> validate() {
		Stream<HasValue<?>> errorFields = view.getBinder().validate().getFieldValidationErrors().stream()
				.map(BindingValidationStatus::getField);
		return errorFields;
	}
	
	private Organization saveOrganization() {
		try {
			Organization organization = view.getOrganization();
			return organizationService.save(organization);
		} catch (ValidationException e) {
			Notification.show("Пожалуйста проверьте заполнение полей: " + e.getMessage(), Type.ERROR_MESSAGE);
			return null;
		} catch (OptimisticLockingFailureException e) {
			Notification.show("Возможно, кто-то еще мог обновить данные. Обновите страницу и повторите попытку.",
					Type.ERROR_MESSAGE);
			return null;
		} catch (Exception e) {
			Notification.show("При сохранении произошла непредвиденная ошибка. Обновите страницу и повторите попытку.",
					Type.ERROR_MESSAGE);
			return null;
		}
	}
	
	public Long getId(String value) {
		String orgId = navigationManager.getParameter(value);
		return "".equals(orgId) ? null : Long.valueOf(orgId);
	}
	
	public Mode getMode() {
		return navigationManager.getMode();
	}
	

}
