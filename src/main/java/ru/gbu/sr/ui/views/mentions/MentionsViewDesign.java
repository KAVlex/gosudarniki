package ru.gbu.sr.ui.views.mentions;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class MentionsViewDesign extends VerticalLayout {
	private static final long serialVersionUID = 1L;

	public MentionsViewDesign() {
		Design.read(this);
	}

}
