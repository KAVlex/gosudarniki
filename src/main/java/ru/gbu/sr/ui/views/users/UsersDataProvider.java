package ru.gbu.sr.ui.views.users;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;

import ru.gbu.sr.backend.data.EmployeeFind;
import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.backend.service.EmployeeService;
import ru.gbu.sr.ui.components.Pagination;

@SpringComponent
@PrototypeScope
public class UsersDataProvider extends PageableDataProvider<Employee, Object> {

    private static final long serialVersionUID = 1L;

    @Autowired
    EmployeeService employeeService;

    private EmployeeFind employee = new EmployeeFind();
    private Pageable pageable= PageRequest.of(0, Pagination.DEFAULT_LIMIT);

    @Override
    protected Page<Employee> fetchFromBackEnd(Query<Employee, Object> query, Pageable pageable) {
        pageable = PageRequest.of(this.pageable.getPageNumber(), this.pageable.getPageSize(), pageable.getSort());
        return employeeService.findAnyMatching(employee, pageable);
    }

    @Override
    protected int sizeInBackEnd(Query<Employee, Object> query) {
        int size = employeeService.findAnyMatching(employee, pageable).getNumberOfElements();
        return size;
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("lastName", SortDirection.ASCENDING));
        return sortOrders;
    }

    public int filter(EmployeeFind employee, Pageable pageable) {
        this.employee = employee;
        this.pageable = pageable;
        refreshAll();
        return (int) employeeService.countAnyMatching(employee);
    }

}
