package ru.gbu.sr.ui.views.projects;

import java.io.Serializable;
import java.util.HashSet;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.OptimisticLockingFailureException;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import ru.gbu.sr.backend.data.entity.Project;
import ru.gbu.sr.backend.data.entity.ProjectEstimate;
import ru.gbu.sr.backend.service.ProjectService;
import ru.gbu.sr.backend.service.UserFriendlyDataException;
import ru.gbu.sr.ui.components.ConfirmPopup;
import ru.gbu.sr.ui.navigation.NavigationManager;
import ru.gbu.sr.ui.views.AbstractCardPresenter;
import ru.gbu.sr.ui.views.Mode;

@SpringComponent
@ViewScope
public class ProjectCardPresenter extends AbstractCardPresenter implements Serializable{

	private static final long serialVersionUID = 1L;
	private final ProjectService serviceService;
	private ProjectCardView view;
	@Autowired
	ApplicationContext applicationContext; 
	
	@Autowired
	public ProjectCardPresenter(NavigationManager navigationManager, ConfirmPopup confirmPopup, ProjectService serviceService) {
		super(navigationManager, confirmPopup);
		this.serviceService = serviceService;
	}
	
	public void init(ProjectCardView view) {
		super.init(view);
		this.view = view;
	}
	
	@Override
	public void enterView(Long id) {
		Project project;
		if (id == null) {
			project = new Project();
			view.setMode(Mode.ADD);
		} else {
			project = serviceService.findById(id);
			if (project == null) {
				view.showNotFound();
				return;
			}
		}
		if (view.estimates != null) {
			view.estimates.addAll(project.getEstimates());
			view.estimatesDataProvider.refreshAll();	
		}
		refreshView(project);
	}
	
	private void refreshView(Project project) {
		view.setService(project);
		Long costTotal = 0l;
		for (ProjectEstimate estimate : view.estimates) {
			costTotal += estimate.getCost();
		}
		
		view.cost.setValue(costTotal.toString());
	}
	
	@Override
	public void cancelPressed() {
		navigationManager.navigateTo(ProjectsView.class);
	}
	
	@Override
	public void savePressed() {
		Project service = save();
		if (service != null) {
			navigationManager.navigateTo(ProjectCardView.class, service.getId().toString(), Mode.EDIT);
		}
	}
	
	@Override
	public void saveExitPressed() {
		Project service = save();
		if (service != null) {
			navigationManager.navigateTo(ProjectsView.class);
		}
	}
	
	@Override
	public void lockPressed() {
		confirmPopup.showConfirmDialog(view, "Вы уверены что ходите отклонить инициативу?", () -> {
			Project project = view.getProject();
			project.setEstimates(new HashSet<ProjectEstimate>(view.estimates));
			project.setStatus("Отклонена");
			project = serviceService.save(project);
			Notification.show("Инициатива отклонена");
			navigationManager.navigateTo(ProjectsView.class);
		});
	}

	@Override
	public void unlockPressed() {
		confirmPopup.showConfirmDialog(view, "Вы уверены что ходите принять инициативу?", () -> {
			Project project = view.getProject();
			project.setEstimates(new HashSet<ProjectEstimate>(view.estimates));
			project.setStatus("Рассмотрено");
			project = serviceService.save(project);
			Notification.show("Инициатива одобрена");
			navigationManager.navigateTo(ProjectsView.class);
		});
	}
	
	public void editPressed() {
		Project service = view.getProject();
		navigationManager.navigateTo(ProjectCardView.class, service.getId().toString(), Mode.EDIT);
	}
	
	private Project save() {
		if (readyToSave()) {
			try {
				Project project = view.getProject();
				project.setEstimates(new HashSet<ProjectEstimate>(view.estimates));
				project.setDefstatus(view.defstatus.getValue());
				project = serviceService.save(project);
				if (project != null) {
					setNoChanges();
					Notification.show("Сохранено " + project.getId());
					return project;
				}
			} catch (UserFriendlyDataException e) {
				Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
			} catch (ValidationException e) {
				Notification.show("Пожалуйста проверьте заполнение полей: " + e.getMessage(), Type.ERROR_MESSAGE);
			} catch (OptimisticLockingFailureException e) {
				Notification.show("Возможно, кто-то еще мог обновить данные. Обновите страницу и повторите попытку.",
						Type.ERROR_MESSAGE);
			} catch (Exception e) {
				Notification.show(
						"При сохранении произошла непредвиденная ошибка. Обновите страницу и повторите попытку.",
						Type.ERROR_MESSAGE);
			}
		}
		return null;
	}
	
}
