package ru.gbu.sr.ui.views.projects;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.renderers.TextRenderer;
import com.vaadin.ui.themes.ValoTheme;

import ru.gbu.sr.backend.data.entity.DefinitionStatus;
import ru.gbu.sr.backend.data.entity.Project;
import ru.gbu.sr.ui.Cacheable;
import ru.gbu.sr.ui.components.Pagination;
import ru.gbu.sr.ui.views.Mode;

@SpringView
@Secured({"Operator", "Executor"})
public class ProjectsView extends ProjectsViewDesign implements View, Cacheable{
	
	private static final long serialVersionUID = 1L;
	
	private final ProjectsPresenter presenter;
	
	@Autowired
	public ProjectsView(ProjectsPresenter presenter) {
		this.presenter = presenter;
	}
	
	@PostConstruct
	public void init() {
		presenter.init(this);
		projectsGrid.setColumns("rating", "cost", "name", "dateCreated", "defstatus");
		
		projectsGrid.getColumn("rating").setCaption("Рейтинг").setResizable(true).setHidable(true).setExpandRatio(1);
		projectsGrid.getColumn("cost").setCaption("Стоимость").setResizable(true).setHidable(true).setExpandRatio(1);
		projectsGrid.getColumn("name").setCaption("Наименование").setResizable(true).setExpandRatio(1).
			setDescriptionGenerator(Project::getName);
		projectsGrid.getColumn("defstatus").setCaption("Статус").setResizable(true).setExpandRatio(1)
		.setRenderer(status -> status == null ? "" : ((DefinitionStatus)status).getLabelName(), new TextRenderer());
		projectsGrid.getColumn("dateCreated").setCaption("Дата поступления на согласование").setResizable(true).setHidable(true).setExpandRatio(1);
		projectsGrid.addComponentColumn(this::buildButtons).setExpandRatio(2);
		
		ok.addClickListener(e -> {
			pagination.firePagedChangedEvent();
		});
		cancel.addClickListener(e -> {
			presenter.getBinder().readBean(null);
			pagination.firePagedChangedEvent();
		});
		add.addClickListener(e -> presenter.newService());
		
	}

	public Grid<Project> getGrid(){
		return projectsGrid;
	}
	
	public Pagination getPagination() {
		return pagination;
	}
	
	private HorizontalLayout buildButtons(Project project) {
    	HorizontalLayout horizontalLayout = new HorizontalLayout();
    	Button view = createButton(VaadinIcons.EYE, "Просмотр", project.getId());
    	Button edit = createButton(VaadinIcons.PENCIL, "Редактирование", project.getId());
    	
    	horizontalLayout.addComponent(view);
    	horizontalLayout.addComponent(edit);
       
    	view.addClickListener(e -> presenter.openServiceCard((Long) e.getButton().getData(), Mode.VIEW));
    	edit.addClickListener(e -> presenter.openServiceCard((Long) e.getButton().getData(), Mode.EDIT));
        return horizontalLayout;
    }
	
	private static Button createButton(VaadinIcons icon, String description, Long id) {
    	Button button = new Button(icon);
    	button.addStyleName(ValoTheme.BUTTON_TINY);
    	button.setDescription(description);
    	button.setData(id);
    	return button;
	}
}
