package ru.gbu.sr.ui.views.mentions;

import javax.annotation.PostConstruct;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewBeforeLeaveEvent;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

@SpringView
@Secured({"Operator", "Executor"})
public class MentionsView extends MentionsViewDesign implements View{

	private static final long serialVersionUID = 1L;

	@Autowired
	public MentionsView() {
	}

	@PostConstruct
	public void init() {

	}
	
	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {

	}

	@Override
	public void beforeLeave(ViewBeforeLeaveEvent event) {
			event.navigate();
	}

}
