package ru.gbu.sr.ui.views.users;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.ValidationException;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ru.gbu.sr.backend.EmployeeRepository;
import ru.gbu.sr.backend.data.EmployeeFind;
import ru.gbu.sr.backend.service.EmployeeService;
import ru.gbu.sr.ui.components.ConfirmPopup;
import ru.gbu.sr.ui.components.Pagination;

import java.io.Serializable;

@SpringComponent
@ViewScope
public class UsersPresenter implements Serializable {

    private static final long serialVersionUID = 1L;

    private UsersView view;
    private UsersDataProvider dataProvider;
    private BeanValidationBinder<EmployeeFind> binder;
    private final ConfirmPopup confirmPopup;
    private final EmployeeService employeeService;

    EmployeeRepository employeeRepository;

	@Autowired
	public UsersPresenter(UsersDataProvider dataProvider, EmployeeRepository employeeRepository,
			ConfirmPopup confirmPopup, EmployeeService employeeService) {
		this.dataProvider = dataProvider;
		this.employeeRepository = employeeRepository;
		this.confirmPopup = confirmPopup;
		this.employeeService = employeeService;
	}

    void init(UsersView view) {
		this.view = view;
		this.view.getGrid().setDataProvider(dataProvider);
		binder = new BeanValidationBinder<>(EmployeeFind.class);
		binder.forField(view.getRoles()).bind((e -> e.getRoles()), (e, r) -> e.setRoles(r));
		binder.bindInstanceFields(view);
		int orgs = findAll(new EmployeeFind(), 0, Pagination.DEFAULT_LIMIT);
		view.getPagination().init(orgs);
		view.getPagination().addPageChangeListener(event -> find(event.pageIndex(), event.limit()));
    }

    public void find(int page, int limit) {
        EmployeeFind employeeFind = new EmployeeFind();
        try {
            binder.writeBean(employeeFind);
        } catch (ValidationException e) {
            Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
            e.printStackTrace();
        }
        int orgs = findAll(employeeFind, page, limit);
        view.getPagination().setTotalCount(orgs);
    }

    private int findAll(EmployeeFind employee, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return dataProvider.filter(employee, pageable);
    }

    public BeanValidationBinder<EmployeeFind> getBinder() {
        return binder;
    }
    
    public void lockPressed(Button.ClickEvent e) {
    	String login = (String) e.getButton().getData();
        confirmPopup.showConfirmDialog(view, "Заблокировать " + login + " пользователя?", () -> {
            if (employeeService.lock(login)) {
                Notification.show("Пользователь заблокирован");
                dataProvider.refreshAll();
            }else {
                Notification.show("Не удалось заблокировать пользователя, обратитесь к администратору", Type.ERROR_MESSAGE);
            }
        });
    }
 
    public void unlockPressed(Button.ClickEvent e) {
    	String login = (String) e.getButton().getData();
        confirmPopup.showConfirmDialog(view, "Восстановить " + login + " пользователя?", () -> {
            if (employeeService.unlock((String) e.getButton().getData())) {
                Notification.show("Пользователь восстановлен");
                dataProvider.refreshAll();
            }else {
                Notification.show("Не удалось восстановить пользователя, обратитесь к администратору", Type.ERROR_MESSAGE);
            }
        });
    }
}
