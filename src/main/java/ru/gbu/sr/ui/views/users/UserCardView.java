package ru.gbu.sr.ui.views.users;

import javax.annotation.PostConstruct;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewBeforeLeaveEvent;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

import ru.gbu.sr.app.security.SecurityUtils;
import ru.gbu.sr.backend.data.Role;
import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.backend.data.entity.Organization;
import ru.gbu.sr.ui.components.ConfirmPopup;
import ru.gbu.sr.ui.utils.CertificateOfEmployeeConvertor;
import ru.gbu.sr.ui.utils.DateToStringConvertor;
import ru.gbu.sr.ui.views.Mode;

import java.util.Collection;
import java.util.Locale;


@SpringView
@Secured({"Operator"})
public class UserCardView extends UserCardViewDesign implements View{

	private static final long serialVersionUID = 1L;
	private Mode mode;
	private final UserCardPresenter presenter;
	private BeanValidationBinder<Employee> binder;
	private final BeanFactory beanFactory;
	private CertificateOfEmployeeConvertor certificateOfEmployeeConvertor;
	private boolean hasChanges;
    private String initialLogin;
    private String initialSnils;
    private String oldPasswordHash;
    private static final String HIDDEN_PASSWORD = "********"; 

	@Autowired
	public UserCardView(UserCardPresenter presenter, BeanFactory beanFactory) {
		this.presenter = presenter;
		this.beanFactory = beanFactory;
	}

	@PostConstruct
	public void init() {
		presenter.init(this);
		binder = new BeanValidationBinder<>(Employee.class);
		bindRole(operator, Role.Operator);
		bindRole(administrator, Role.Administrator);
		bindRole(executor, Role.Executor);
		bindRole(declarant, Role.Declarant);
		bindRole(supervisor, Role.Supervisor);
		bindRole(supersupervisor, Role.SuperSupervisor);
		bindRole(manager, Role.Manager);
		binder.forField(dateUpdated).withConverter(new DateToStringConvertor()).bind("dateUpdated");
		binder.forField(birthDate).withConverter(new LocalDateToDateConverter()).bind("birthDate");
		certificateOfEmployeeConvertor = new CertificateOfEmployeeConvertor();
		binder.forField(cert).withConverter(certificateOfEmployeeConvertor).bind("certificate");
		
		binder.forField(updater).bind(employee -> { 
			return (employee.getUpdater() != null) ? employee.getUpdater().getLogin() : "";
		}, null);
		organization.setItemCaptionGenerator(Organization::getName);
		addValidators();
		binder.bindInstanceFields(this);
		binder.addValueChangeListener(e -> hasChanges = true);

		generateLogin.addClickListener(e -> presenter.generateLoginPressed(lastName.getValue(), firstName.getValue(),
				patronymic.getValue(), login));
		generatePassword.addClickListener(e -> presenter.generatePasswordPressed(passwordHash));
		clearCert.addClickListener(e -> presenter.clearCertPressed(certificateOfEmployeeConvertor, cert));
		cancel.addClickListener(e -> presenter.cancelPressed());
		edit.addClickListener(e -> presenter.editPressed());
		save.addClickListener(e -> presenter.savePressed());
		saveExit.addClickListener(e -> presenter.saveExitPressed());
		lock.addClickListener(e -> presenter.lockPressed());
		unlock.addClickListener(e -> presenter.unlockPressed());
		
		generateLogin.setEnabled(false);
		firstName.addValueChangeListener(this::changeGenarateLoginEnabled);
		lastName.addValueChangeListener(this::changeGenarateLoginEnabled);
		birthDate.setDateFormat("dd/MM/yyyy");
		birthDate.setLocale(new Locale("ru", "RU"));
	}
	
	private void changeGenarateLoginEnabled(ValueChangeEvent<String> event) {
		boolean enabled = !firstName.isEmpty() && !lastName.isEmpty() && getEmployee().isNew();
		generateLogin.setEnabled(enabled);
		generateLogin.setDescription(enabled ? "" : "Заполните основную информацию");
	}
	
	private void bindRole(CheckBox checkBox, Role role) {
		binder.forField(checkBox).bind(employee -> containsRole(employee, role),
				(employee, aBoolean) -> addRole(employee, aBoolean, role));
	}
	
	private boolean containsRole(Employee employee, Role role) {
		return employee.getRoles().contains(role) ? Boolean.TRUE : Boolean.FALSE;
	}
	
	private void addRole(Employee employee, Boolean aBoolean, Role role) {
		if (aBoolean) {
			employee.getRoles().add(role);
		} else {
			if (employee.getRoles().contains(role))
				employee.getRoles().remove(role);
		}
		swithRole(role, aBoolean);
	}
	
	/**
	 * Либо Админ либо Оператор либо иная другая роль
	 * @param role
	 * @param disableOther
	 */
	private void swithRole(Role role, Boolean disableOther) {
		switch (role) {
		case Administrator:
			executor.setEnabled(!disableOther);
			declarant.setEnabled(!disableOther);
			supervisor.setEnabled(!disableOther);
			supersupervisor.setEnabled(!disableOther);
			manager.setEnabled(!disableOther);
			operator.setEnabled(!disableOther);
		default:
			disableOther = disableOther || executor.getValue() || declarant.getValue() || supervisor.getValue()
					|| supersupervisor.getValue() || manager.getValue() || operator.getValue();
			administrator.setEnabled(!disableOther || role.equals(Role.Administrator));
			break;
		}
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {
		String usrLogin = presenter.getLogin(event.getParameters());
		setMode(presenter.getMode());
		if (mode == null) {
			Notification.show("Неверный режим, обратитесь к администратору", Notification.Type.ERROR_MESSAGE);
		}else {
			presenter.enterView(usrLogin);
		}
	}

	private void addValidators(){
        binder.forMemberField(organization).withValidator((value, c) -> {
            return organization.getValue() != null ? ValidationResult.ok() :
                    ValidationResult.error("Необходимо выбрать 'Организацию'");
        });
		addValidator(lastName, "Значение поля 'Фамилия' должно содержать не менее 2 символов", "^\\S+[\\S\\s]*\\S+$");
		addValidator(firstName, "Значение поля 'Имя' должно содержать не менее 2 символов", "^\\S+[\\S\\s]*\\S+$");
		addValidator(email, "Поле 'e-mail' заполнено неверно", "^$|^([a-zA-Z0-9_\\.\\-+])+@[a-zA-Z0-9-.]+\\.[a-zA-Z0-9-]{2,}$");

		binder.forMemberField(login).withValidator(
				new RegexpValidator("Значение поля 'Логин' должно содержать от 4 до 20 цифр и букв латинского алфавита",
						"(^[a-zA-Z0-9_.]{4,20}$)"))
		.withValidator((s, v) -> {
							return presenter.existLogin(s) && (initialLogin == null || !initialLogin.equals(s))
									? ValidationResult.error("Запись с таким логином уже существует")
									: ValidationResult.ok();
						});
		binder.forField(passwordHash).withValidator(new RegexpValidator(
				"Значение поля 'Пароль' не должно содержать менее 8 непробельных символов", "(^\\S{8,}$)"));
		binder.forMemberField(snils).withValidator(new RegexpValidator("Значение поля 'СНИЛС' должно содержать 11 цифр", "^[0-9]{11}$"))
		.withValidator((s, v) -> {
			return presenter.existSnils(s) && (initialSnils == null || !initialSnils.equals(s))
					? ValidationResult.error("Запись с таким номером СНИЛС уже существует")
					: ValidationResult.ok();
		});
		addValidator(inn, "Значение поля 'ИНН' должно содержать 12 цифр", "^[0-9]{12}$");
	}

	private Binder.BindingBuilder<Employee, String> addValidator(AbstractTextField field, String text, String regexp) {
		return binder.forMemberField(field).withValidator(new RegexpValidator(text, regexp));
	}

	public void setEmployee(Employee employee) {
		binder.setBean(employee);
		dateUpdated.setReadOnly(true);
		cert.setReadOnly(true);
		
        initialLogin = employee.getLogin();
        initialSnils = employee.getSnils();
        oldPasswordHash = employee.getPasswordHash();
		
		lock.setVisible(
				!employee.isNew() && !employee.isLocked() && !employee.getLogin().equals(SecurityUtils.getUsername()));
		unlock.setVisible(!employee.isNew() && employee.isLocked());
		if (employee.getOrganization() != null && employee.getOrganization().isLocked()) {
			unlock.setEnabled(false);
			unlock.setDescription("Нельзя разблокировать пользователя! Он относится к заблокированной организации");
		}
		login.setEnabled(employee.isNew());
		if (!employee.isNew())
			passwordHash.setValue(HIDDEN_PASSWORD);
		clearCert.setEnabled(employee.getCertificate() != null && !mode.equals(Mode.VIEW));
		if (employee.getRoles() != null && employee.getRoles().size() > 0) {
			swithRole(employee.getRoles().iterator().next(), true);
		}
		if (employee.isLocked()) {
			edit.setVisible(false);
		}
		hasChanges = false;
	}
	
	protected String getPassword() {
		return isNewPassword() ? passwordHash.getValue() : oldPasswordHash;
	}
	
	protected boolean isNewPassword() {
		return passwordHash.getValue() != HIDDEN_PASSWORD;
	}

	protected Employee getEmployee() {
		return binder.getBean();
	}

	protected BeanValidationBinder<Employee> getBinder(){
		return binder;
	}

	public void showNotFound() {
		removeAllComponents();
		addComponent(new Label("Пользователь не найден"));
	}

	public boolean containsUnsavedChanges() {
		return hasChanges;
	}

	public void setNoChanges() {
		hasChanges = false;
	}

	public ConfirmPopup getConfirmPopup() {
		return beanFactory.getBean(ConfirmPopup.class);
	}

	public void setOrganizationItems(Collection<Organization> items) {
		organization.setItems(items);
	}

	@Override
	public void beforeLeave(ViewBeforeLeaveEvent event) {
		if (!containsUnsavedChanges()) {
			event.navigate();
		} else {
			ConfirmPopup confirmPopup = getConfirmPopup();
			confirmPopup.showLeaveViewConfirmDialog(this, event::navigate);
		}
	}

	public void setMode(Mode mode) {
		this.mode = mode;
		generatePassword.setEnabled(!mode.equals(Mode.VIEW));
		edit.setVisible(mode.equals(Mode.VIEW));
		save.setVisible(!mode.equals(Mode.VIEW));
		saveExit.setVisible(!mode.equals(Mode.VIEW));
		binder.setReadOnly(mode.equals(Mode.VIEW));
		switch (mode) {
			case ADD:
				modeLabel.setValue("Создание пользователя");
				break;
			case EDIT:
				modeLabel.setValue("Редактирование пользователя");
				break;
			default:
				modeLabel.setValue("Просмотр пользователя");
				break;
		}
	}

}
