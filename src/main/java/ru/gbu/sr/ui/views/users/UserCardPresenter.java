package ru.gbu.sr.ui.views.users;

import com.vaadin.data.BindingValidationStatus;
import com.vaadin.data.HasValue;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.security.crypto.password.PasswordEncoder;

import ru.gbu.sr.backend.data.entity.Employee;
import ru.gbu.sr.backend.service.EmployeeService;
import ru.gbu.sr.backend.service.OrganizationService;
import ru.gbu.sr.backend.service.UserFriendlyDataException;
import ru.gbu.sr.ui.components.ConfirmPopup;
import ru.gbu.sr.ui.navigation.NavigationManager;
import ru.gbu.sr.ui.utils.CertificateOfEmployeeConvertor;
import ru.gbu.sr.ui.utils.Generator;
import ru.gbu.sr.ui.views.Mode;

import javax.validation.ValidationException;
import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

@SpringComponent
@ViewScope
public class UserCardPresenter implements Serializable {

    private static final long serialVersionUID = 1L;

    private final EmployeeService employeeService;
    private final OrganizationService organizationService;
    private final PasswordEncoder passwordEncoder;
    private UserCardView view;
    private final NavigationManager navigationManager;

    @Autowired
	public UserCardPresenter(EmployeeService employeeService, OrganizationService organizationService,
			NavigationManager navigationManager, PasswordEncoder passwordEncoder) {
        this.employeeService = employeeService;
        this.organizationService = organizationService;
        this.navigationManager = navigationManager;
        this.passwordEncoder = passwordEncoder;
    }

    public void init(UserCardView view) {
        this.view = view;
    }

    public void enterView(String login) {
        Employee usr;
        if (login == null || login.isEmpty()) {
            usr = new Employee();

            view.setMode(Mode.ADD);
        } else {
            usr = employeeService.findByLogin(login);
            if (usr == null) {
                view.showNotFound();
                return;
            }
        }

        view.setOrganizationItems(organizationService.findAllActive());
		String org = getOrgId();
		if (org != null && !org.isEmpty()) {
			usr.setOrganization(organizationService.findById(new Long(org)));
		}

        refreshView(usr);
    }

    private void refreshView(Employee employee) {
        view.setEmployee(employee);
    }
    
    public void generateLoginPressed(String surname, String name, String patronymic, TextField field) {
    	try {
        	String login = Generator.generateLogin(surname, name, patronymic, employeeService);
        	field.setValue(login);	
		} catch (UserFriendlyDataException e) {
			Notification.show(e.getMessage());
		}
    }
    
    public void generatePasswordPressed(TextField field) {
    	field.setValue(Generator.generatePassword());
    }
       
    public void clearCertPressed(CertificateOfEmployeeConvertor certificateOfEmployeeConvertor, TextField field) {
    	ConfirmPopup confirmPopup = view.getConfirmPopup();
        confirmPopup.showConfirmDialog(view, "Вы уверены, что хотите сбросить привязку к сертификату?", () -> {
        	certificateOfEmployeeConvertor.setCertificateWasRemoved(true);
        	field.clear();
        });
    }

    public void cancelPressed() {
        navigationManager.navigateTo(UsersView.class);
    }
    
	public void editPressed() {
		Employee employee = view.getEmployee();
		navigationManager.navigateTo(UserCardView.class, employee.getLogin(), Mode.EDIT);
	}

    public void savePressed() {
    	Employee employee = save();
    	if (employee != null) {
    		navigationManager.navigateTo(UserCardView.class, employee.getLogin(), Mode.EDIT);
    	}
    }
    
    public void saveExitPressed() {
    	Employee employee = save();
    	if (employee != null) {
    		navigationManager.navigateTo(UsersView.class);
    	}
    }
    
    private Employee save() {
        if (view.containsUnsavedChanges()) {
            Optional<HasValue<?>> firstErrorField = validate().findFirst();
            if (firstErrorField.isPresent()) {
                ((Component.Focusable) firstErrorField.get()).focus();
                return null;
            }
            Employee employee = saveEmployee();
            if (employee != null) {
                view.setNoChanges();
                Notification.show("Сохранено " + employee.getLogin());
                return employee;
            }
        }else {
            Notification.show("Нет изменений");
        }
        return null;
    }

    public void lockPressed() {
        Employee employee = view.getEmployee();
        ConfirmPopup confirmPopup = view.getConfirmPopup();
        confirmPopup.showConfirmDialog(view, "Заблокировать " + employee.getLogin() + " пользователя?", () -> {
            if (employeeService.lock(employee)) {
                Notification.show("Пользователь заблокирован");
                navigationManager.navigateTo(UsersView.class);
            }else {
                Notification.show("Не удалось заблокировать пользователя, обратитесь к администратору", Notification.Type.ERROR_MESSAGE);
            }
        });
    }

    public void unlockPressed() {
        Employee employee = view.getEmployee();
        ConfirmPopup confirmPopup = view.getConfirmPopup();
        confirmPopup.showConfirmDialog(view, "Восстановить " + employee.getLogin() + " пользователя?", () -> {
            if (employeeService.unlock(employee)) {
                Notification.show("Пользователь восстановлен");
                navigationManager.navigateTo(UsersView.class);
            }else {
                Notification.show("Не удалось восстановить пользователя, обратитесь к администратору", Notification.Type.ERROR_MESSAGE);
            }
        });
    }

    public void navigateTo(Class<? extends View> targetView, Object parameter, Mode mode) {
        navigationManager.navigateTo(targetView, parameter, mode);
    }

    public Stream<HasValue<?>> validate() {
        Stream<HasValue<?>> errorFields = view.getBinder().validate().getFieldValidationErrors().stream()
                .map(BindingValidationStatus::getField);
        return errorFields;
    }

    private Employee saveEmployee() {
        try {
            Employee employee = view.getEmployee();
            if (employee.getRoles().size() == 0) {
            	throw new ValidationException("Необходимо выбрать роль");
            }
			employee.setPasswordHash(
					view.isNewPassword() ? passwordEncoder.encode(view.getPassword()) : view.getPassword());
            return employeeService.save(employee);
        } catch (ValidationException e) {
            Notification.show("Пожалуйста проверьте заполнение полей: " + e.getMessage(), Notification.Type.ERROR_MESSAGE);
            return null;
        } catch (OptimisticLockingFailureException e) {
            Notification.show("Возможно, кто-то еще мог обновить данные. Обновите страницу и повторите попытку.",
                    Notification.Type.ERROR_MESSAGE);
            return null;
        } catch (Exception e) {
            Notification.show("При сохранении произошла непредвиденная ошибка. Обновите страницу и повторите попытку.",
                    Notification.Type.ERROR_MESSAGE);
            return null;
        }
    }

    public String getLogin(String value) {
        String usrLogin = navigationManager.getParameter(value);
        return usrLogin;
    }

    public Mode getMode() {
        return navigationManager.getMode();
    }
    
	public String getOrgId() {
		return navigationManager.getStateParameterMap() != null ? navigationManager.getStateParameterMap().get("org")
				: null;
	}
    
    public boolean existLogin(String login){
        return employeeService.existLogin(login);
    }
 
    public boolean existSnils(String snils){
        return employeeService.existSnils(snils);
    }
}
