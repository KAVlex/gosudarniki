package ru.gbu.sr.ui.views;

import com.vaadin.data.ValueProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;

public class LockedValueProvider implements ValueProvider<Boolean, Label>{

	private static final long serialVersionUID = 1L;

	@Override
	public Label apply(Boolean source) {
		return buildLockedLabel(source);
	}

	private Label buildLockedLabel(Boolean source) {
        Label image = new Label();
        image.setContentMode(ContentMode.HTML);
        image.setValue(VaadinIcons.CIRCLE.getHtml());
        image.addStyleName(source ? "locked" : "active");
        image.setDescription(source ? "Заблокировано" : "Активно");
        return image;
	}
}
