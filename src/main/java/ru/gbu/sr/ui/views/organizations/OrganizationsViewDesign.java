package ru.gbu.sr.ui.views.organizations;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateTimeField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

import ru.gbu.sr.backend.data.entity.Organization;
import ru.gbu.sr.ui.components.Pagination;

@DesignRoot
public class OrganizationsViewDesign extends VerticalLayout{
	
	private static final long serialVersionUID = 1L;
	protected Panel paginationPanel; 
	protected Grid<Organization> organizationsGrid;
	protected Pagination pagination;
	protected TextField name;
	protected TextField id;
	protected TextField registryNumber;
	protected CheckBox active;
	protected DateTimeField startDate;
	protected DateTimeField endDate;
	protected TextField oktmo;
	protected TextField inn;
	protected Button ok;
	protected Button cancel;
	protected Button add;

	public OrganizationsViewDesign() {
		Design.read(this);
	}

}
