package ru.gbu.sr.ui.views.organizations;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.vaadin.addon.pagination.PaginationChangeListener;
import com.vaadin.addon.pagination.PaginationResource;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.ValidationException;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import ru.gbu.sr.backend.data.OrganizationFind;
import ru.gbu.sr.backend.service.OrganizationService;
import ru.gbu.sr.ui.components.ConfirmPopup;
import ru.gbu.sr.ui.components.Pagination;
import ru.gbu.sr.ui.navigation.NavigationManager;
import ru.gbu.sr.ui.views.Mode;
import ru.gbu.sr.ui.views.users.UserCardView;

@SpringComponent
@ViewScope
public class OrganizationsPresenter implements Serializable {

	private static final long serialVersionUID = 1L;

	private OrganizationsView view;
	private OrganizationsDataProvider dataProvider;
	private BeanValidationBinder<OrganizationFind> binder;

	private final OrganizationService organizationService;
	private final NavigationManager navigationManager;
	private final ConfirmPopup confirmPopup;
	
	@Autowired
	public OrganizationsPresenter(OrganizationsDataProvider dataProvider, OrganizationService organizationService, 
			NavigationManager navigationManager, ConfirmPopup confirmPopup) {
		this.dataProvider = dataProvider;
		this.organizationService = organizationService;
		this.navigationManager = navigationManager;
		this.confirmPopup = confirmPopup;
	}

	void init(OrganizationsView view) {
		this.view = view;
		this.view.getGrid().setDataProvider(dataProvider);
		binder = new BeanValidationBinder<>(OrganizationFind.class);
		binder.bindInstanceFields(view);
		int orgs = findAll(new OrganizationFind(), 0, Pagination.DEFAULT_LIMIT);
		view.getPagination().init(orgs);
		view.getPagination().addPageChangeListener(new PaginationChangeListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void changed(PaginationResource event) {
			    find(event.pageIndex(), event.limit());
			}
		});
	}
	
	public void find(int page, int limit) {
		OrganizationFind organizationFind = new OrganizationFind();
		try {
			binder.writeBean(organizationFind);
		} catch (ValidationException e) {
			Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
	    int orgs = findAll(organizationFind, page, limit);
	    view.getPagination().setTotalCount(orgs);
	}

	private int findAll(OrganizationFind organization, int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
		return dataProvider.filter(organization, pageable);
	}
	
	public BeanValidationBinder<OrganizationFind> getBinder() {
		return binder;
	}
	
	public void newOrganization() {
		navigationManager.navigateTo(OrganizationCardView.class);
	}
	
	public void openOrfanizationCard(Long id, Mode mode) {
		navigationManager.navigateTo(OrganizationCardView.class, id, mode);
	}
	
	public void addUserPressed(Long id) {
		navigationManager.navigateTo(UserCardView.class, "&org=" + id, Mode.ADD);
	}
	
	public void lockPressed(ClickEvent e) {
		confirmPopup.showConfirmDialog(view, "Заблокировать организацию и имеющихся в ней пользователей?", () -> {
			if (organizationService.lock((Long)e.getButton().getData())) {
				Notification.show("Организация заблокирована");
				dataProvider.refreshAll();					
			}else {
				Notification.show("Не удалось заблокировать организацию, обратитесь к администратору", Type.ERROR_MESSAGE);
			}
		});
	}
	
	public void unlockPressed(ClickEvent e) {
		confirmPopup.showConfirmDialog(view, "Восстановить организацию? Пользователей организации необходимо добавить вручную!", () -> {
			if (organizationService.unlock((Long)e.getButton().getData())) {
				Notification.show("Организация восстановлена");
				dataProvider.refreshAll();					
			}else {
				Notification.show("Не удалось восстановить организацию, обратитесь к администратору", Type.ERROR_MESSAGE);
			}
		});
	}
}
