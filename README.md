# Running the project

`mvn spring-boot:run`

Wait for the application to start

Open http://localhost:8090/ to view the application.

Default credentials are demooperator/demooperator for admin access and
demoexecutor/demoexecutor for normal user access.

# Running the project as an executable jar

The project is configured to automatically make the build artifact runnable using `java -jar`.
By default you can thus also run the project by executing the war file:
```
java -jar target/web-app-1.0-SNAPSHOT.war
```

If you want to produce a `jar` file instead of a `war` file, change the packaging type in `pom.xml` to `<packaging>jar</packaging>`.


# Developing the project

The project can be imported into the IDE of your choice as a Maven project

The views are created using Vaadin Designer. To edit the views visually,
you need to install the Vaadin Designer plug-in.

In Eclipse, open Marketplace, search for "vaadin" and install Vaadin
Designer 2.x

In IntelliJ, go to "Preferences" -> "Plugins" -> "Browse Repositories",
search for "Vaadin Designer 2" and install "Vaadin Designer"
